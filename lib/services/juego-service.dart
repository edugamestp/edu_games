import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer' as dev;

import 'package:edu_games/models/Juegos.dart';

class JuegoService {
  CollectionReference juegoCollec = FirebaseFirestore.instance.collection(Juego.collectionId);

  Future<List<Juego>> getJuegos() async {
    try {
      QuerySnapshot querySnap = await juegoCollec.get();
      List<Juego> juegos = querySnap.docs.map((doc) => Juego.fromJSON(doc.id, doc.data())).toList();
      return juegos;
    } catch (e) {
      dev.log(e.toString(), name: 'juegoService.getJuegos');
      throw Exception(e);
    }
  }
}