import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_games/models/Usuario.dart';
import 'dart:developer' as dev;
import 'package:flutter_string_encryption/flutter_string_encryption.dart';

class AuthService {
  CollectionReference usuariosCollection =
      FirebaseFirestore.instance.collection(Usuario.collectionId);

  String key = 'null';
  PlatformStringCryptor cryptor;

  Future<String> postUsuario(Usuario usuario) async {
    try {
      cryptor = PlatformStringCryptor();
      final salt = await cryptor.generateSalt();
      key = await cryptor.generateKeyFromPassword(usuario.contrasenya, salt);
      String encryptedPass = await cryptor.encrypt(usuario.contrasenya, key);
      usuario.contrasenya = encryptedPass;
      usuario.contrasenyaKey = key;
      DocumentReference docRef = await usuariosCollection.add(usuario.toJSON());
      return docRef.id;
    } catch (e) {
      dev.log(e.toString(), name: 'authService.post');
      throw Exception(e);
    }
  }

  Future<Usuario> loginUsuario(Usuario usuario) async {
    try {
      QuerySnapshot querySnapshot = await usuariosCollection
        .where('dni', isEqualTo: usuario.dni)
        .get();

      Usuario response;

      if (querySnapshot.docs.length > 0) {
        response = querySnapshot.docs
            .map((doc) => Usuario.fromJSON(doc.id, doc.data()))
            .toList()[0];
      } else {
        return null;
      }

      cryptor = PlatformStringCryptor();
      String decryptedPass =
          await cryptor.decrypt(response.contrasenya, response.contrasenyaKey);
      if (usuario.contrasenya == decryptedPass)
        return response;
      else
        return null;
    } catch (e) {
      dev.log(e.toString(), name: 'authService.login');
      throw Exception(e);
    }
  }

  Future<bool> updateContrasenya(
      Usuario usuario, String oldContrasenya, String newContrasenya) async {
    try {
      QuerySnapshot querySnapshot =
          await usuariosCollection.where('dni', isEqualTo: usuario.dni).get();
      Usuario response = querySnapshot.docs
          .map((doc) => Usuario.fromJSON(doc.id, doc.data()))
          .toList()[0];
      cryptor = PlatformStringCryptor();
      String decryptedPass =
          await cryptor.decrypt(response.contrasenya, response.contrasenyaKey);

      if (oldContrasenya != decryptedPass) return false;

      final salt = await cryptor.generateSalt();
      key = await cryptor.generateKeyFromPassword(newContrasenya, salt);
      String encryptedPass = await cryptor.encrypt(newContrasenya, key);
      usuario.contrasenya = encryptedPass;
      usuario.contrasenyaKey = key;
      await usuariosCollection.doc(usuario.id).set(usuario.toJSON());
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'authService.updateContrasenya');
      throw Exception(e);
    }
  }
}
