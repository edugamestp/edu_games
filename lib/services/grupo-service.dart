import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/models/Usuario.dart';
import 'dart:developer' as dev;
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/services/usuario-service.dart';

class GrupoService {
  CollectionReference grupoCollec =
      FirebaseFirestore.instance.collection(Grupo.collectionId);
  final SharedPreferenceService prefs = SharedPreferenceService();
  final UsuarioService usuarioService = UsuarioService();

  Future<bool> postGrupo(Grupo grupo) async {
    try {
      grupo.idDocente = prefs.userId;
      grupo.dniDocente = prefs.userDni;
      DocumentReference docRef = await grupoCollec.add(grupo.toJSON());
      return docRef.id.isNotEmpty;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.post');
      throw Exception(e);
    }
  }

  Future<bool> putGrupo(Grupo grupo) async {
    try {
      await grupoCollec.doc(grupo.id).set(grupo.toJSON());
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.put');
      throw Exception(e);
    }
  }

  Future<List<Grupo>> getGruposByDocente() async {
    try {
      QuerySnapshot querySnap =
          await grupoCollec.where('idDocente', isEqualTo: prefs.userId).get();
      List<Grupo> grupos = querySnap.docs
          .map((doc) => Grupo.fromJSON(doc.id, doc.data()))
          .toList();
      return grupos;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.getByDocente');
      throw Exception(e);
    }
  }

  Future<Grupo> getGrupoById(String id) async {
    try {
      DocumentSnapshot docSnap = await grupoCollec.doc(id).get();
      Grupo grupo = Grupo.fromJSON(id, docSnap.data());
      return grupo;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.getById');
      throw Exception(e);
    }
  }

  Future<String> addAlumnoToGrupo(Grupo grupo, String alumnoDni) async {
    try {
      Usuario foundAlumno =
          await usuarioService.getUsuarioAlumnoByDni(alumnoDni);
      if (foundAlumno == null) return 'no-existe';
      if (foundAlumno.role != 'alumno' || foundAlumno.grupoId.length > 0) return 'en-grupo';
      foundAlumno.grupoId = grupo.id;
      bool wasUpdated = await usuarioService.updateUsuario(foundAlumno);
      if (!wasUpdated) return 'error';
      grupo.alumnos.add('${foundAlumno.nombres} ${foundAlumno.apellidos}');
      grupo.actualAlumnos += 1;
      await grupoCollec.doc(grupo.id).set(grupo.toJSON());
      return 'agregado';
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.addAlumno');
      throw Exception(e);
    }
  }

  Future<Grupo> updateAlumnos(Grupo grupo, String alumno) async {
    try {
      grupo.alumnos.remove(alumno);
      grupo.actualAlumnos -= 1;
      final updatedAlumno =
          await usuarioService.updateAlumnoGrupo(grupo.id, alumno);
      if (!updatedAlumno) return null;
      await grupoCollec.doc(grupo.id).set(grupo.toJSON());
      return grupo;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.updateAlumnos');
      throw Exception(e);
    }
  }

  Future<bool> deleteGrupo(Grupo grupo) async {
    try {
      await usuarioService.updateAlumnosGrupo(grupo.id);
      await grupoCollec.doc(grupo.id).delete();
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'grupoService.deleteGrupo');
      throw Exception(e);
    }
  }
}
