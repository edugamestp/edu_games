import 'package:edu_games/models/Usuario.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceService {
  static final SharedPreferenceService _instance = SharedPreferenceService._internal();
  SharedPreferenceService._internal();
  
  factory SharedPreferenceService() => _instance;

  SharedPreferences _prefs;

  initData() async {
    _prefs = await SharedPreferences.getInstance();
  }

  clearData() async {
    await _prefs.clear();
  }

  saveData(Usuario usuario) {
    userId = usuario.id;
    userDni = usuario.dni;
    userGenero = usuario.genero;
    userRole = usuario.role;
    userGrupoId = usuario.grupoId;
  }

  String get userId => _prefs.getString('userId') ?? '';

  set userId(String id) {
    _prefs.setString('userId', id);
  }

  String get userDni => _prefs.getString('userDni') ?? '';

  set userDni(String dni) {
    _prefs.setString('userDni', dni);
  }

  String get userGenero => _prefs.getString('userGenero') ?? '';

  set userGenero(String genero) {
    _prefs.setString('userGenero', genero);
  }

  String get userRole => _prefs.getString('userRole') ?? '';

  set userRole(String role) {
    _prefs.setString('userRole', role);
  }

  String get userGrupoId => _prefs.getString('userGrupoId') ?? '';

  set userGrupoId(String grupoId) {
    _prefs.setString('userGrupoId', grupoId);
  }
}