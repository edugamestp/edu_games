import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer' as dev;
import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/services/evaluacion-alumno-service.dart';
import 'package:edu_games/services/grupo-service.dart';
import 'package:edu_games/services/usuario-service.dart';

class EvaluacionSevice {
  CollectionReference evaluacionCollec =
      FirebaseFirestore.instance.collection(Evaluacion.collectionId);

  final UsuarioService _usuarioService = UsuarioService();
  final EvaluacionAlumnoService _evalAlumnoService = EvaluacionAlumnoService();
  final GrupoService _grupoService = GrupoService();

  Future<List<Evaluacion>> getEvaluacionesByDocenteId(String docenteId) async {
    try {
      QuerySnapshot querySnap = await evaluacionCollec
        .where('docenteId', isEqualTo: docenteId)
        .get();

      List<Evaluacion> evaluaciones = querySnap.docs
          .map((doc) => Evaluacion.fromJSON(doc.id, doc.data()))
          .toList();

      return evaluaciones;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.getByDocenteId');
      throw Exception(e);
    }
  }

  Future<List<Evaluacion>> getQueryEvaluaciones(String searchKey, String docenteId) async {
    try {
      QuerySnapshot querySnap;

      if(searchKey.length > 0 ) {
        querySnap = await evaluacionCollec
          .where('docenteId', isEqualTo: docenteId)
          .where('nombre', isGreaterThanOrEqualTo: searchKey) 
          .orderBy('nombre')       
          .get();
      } else {
         querySnap = await evaluacionCollec
          .where('docenteId', isEqualTo: docenteId)
          .orderBy('nombre')       
          .get();
      }
      
      List<Evaluacion> evaluaciones = querySnap.docs
          .map((doc) => Evaluacion.fromJSON(doc.id, doc.data()))
          .toList();

      return evaluaciones;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.getByDocenteId');
      throw Exception(e);
    }
  }

  Future<bool> postEvaluacion(Evaluacion evaluacion) async {
    try {
      final grupo = await _grupoService.getGrupoById(evaluacion.grupoId);
      evaluacion.alumnos = grupo.actualAlumnos;
      DocumentReference docRef = await evaluacionCollec.add(evaluacion.toJSON());
      List<Usuario> usuarios = await _usuarioService.getUsuariosByGrupoId(evaluacion.grupoId);
      if (usuarios != null) {
        usuarios.forEach((us) async {
          EvaluacionAlumno evaluacionAlumno = EvaluacionAlumno();
          evaluacionAlumno.alumnoId = us.id;
          evaluacionAlumno.evaluacionId = docRef.id;
          evaluacionAlumno.nombreEvaluacion = evaluacion.nombre;
          evaluacionAlumno.juegoId = evaluacion.juegoId;
          await _evalAlumnoService.postEvaluacionAlumno(evaluacionAlumno);
        });
      }
      return docRef.id.isNotEmpty;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.postEvaluacion');
      throw Exception(e);
    }
  }

  Future<bool> putEvaluacion(Evaluacion evaluacion) async {
    try {
      await evaluacionCollec.doc(evaluacion.id).set(evaluacion.toJSON());
      final response = await _evalAlumnoService.updateEvaluacionesAlumno(evaluacion);
      if (response)
        return true;
      else
        return false;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.putEvaluacion');
      throw Exception(e);
    }
  }

  Future<bool> deleteEvaluacion(Evaluacion evaluacion) async {
    try {
      await _evalAlumnoService.deleteEvaluacionesAlumnoByEvaluacionId(evaluacion.id);
      await evaluacionCollec.doc(evaluacion.id).delete();
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.deleteEvaluacion');
      throw Exception(e);
    }
  }

  activateEvaluacion(Evaluacion evaluacion) async {
    try {
      evaluacion.activado = true;
      await evaluacionCollec.doc(evaluacion.id).set(evaluacion.toJSON());
      final response = await _evalAlumnoService.activateEvaluacionesAlumnoByEvaluacionId(evaluacion.id);
      if (response)
        return true;
      else
        return false;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionService.activateEvaluacion');
      throw Exception(e);
    }
  }
}
