import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer' as dev;

import 'package:edu_games/models/Pregunta.dart';

class PreguntaService {
  CollectionReference preguntaCollec =
      FirebaseFirestore.instance.collection(Pregunta.collectionId);

  Future<List<Pregunta>> getPreguntas() async {
    try {
      QuerySnapshot querySnap = await preguntaCollec.get();

      List<Pregunta> preguntas = querySnap.docs
          .map((doc) => Pregunta.fromJSON(doc.id, doc.data()))
          .toList();

      return preguntas;
    } catch (e) {
      dev.log(e.toString(), name: 'preguntaService.get');
      throw Exception(e);
    }
  }
}
