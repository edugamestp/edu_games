import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_games/models/Evaluacion.dart';
import 'dart:developer' as dev;
import 'package:edu_games/models/EvaluacionAlumno.dart';

class EvaluacionAlumnoService {
  CollectionReference evaAlumnoCollec = FirebaseFirestore.instance.collection(EvaluacionAlumno.collectionId);

  Future<List<EvaluacionAlumno>> getEvaluacionesByAlumno(String alumnoId) async {
    try {
      QuerySnapshot querySnap = await evaAlumnoCollec
        .where('alumnoId', isEqualTo: alumnoId)
        .get();
        
      List<EvaluacionAlumno> evaluacionesAlumno = querySnap.docs
        .map((doc) => EvaluacionAlumno.fromJSON(doc.id, doc.data()))
        .toList();

      return evaluacionesAlumno;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.getByAlumno');
      throw Exception(e);
    }
  }

  Future<List<EvaluacionAlumno>> getEvaluacionesActivasByAlumno(String alumnoId) async {
    try {
      QuerySnapshot querySnap = await evaAlumnoCollec
        .where('alumnoId', isEqualTo: alumnoId)
        .where('activado', isEqualTo: true)
        .where('estado', isEqualTo: 'proceso')
        .get();
        
      List<EvaluacionAlumno> evaluacionesAlumno = querySnap.docs
        .map((doc) => EvaluacionAlumno.fromJSON(doc.id, doc.data()))
        .toList();

      return evaluacionesAlumno;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.getByAlumno');
      throw Exception(e);
    }
  }

  Future<bool> updateEvaluacionesAlumno(Evaluacion evaluacion) async {
    try {
      QuerySnapshot querySnap = await evaAlumnoCollec
        .where('evaluacionId', isEqualTo: evaluacion.id)
        .get();

      WriteBatch batch = FirebaseFirestore.instance.batch();

      querySnap.docs.forEach((doc) { 
        EvaluacionAlumno evaluacionAlumno = EvaluacionAlumno.fromJSON(doc.id, doc.data());
        evaluacionAlumno.juegoId = evaluacion.juegoId;
        evaluacionAlumno.nombreEvaluacion = evaluacion.nombre;
        batch.update(doc.reference, evaluacionAlumno.toJSON());
      });

      await batch.commit();
      return true;
    } catch(e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.getByEvaluacionId');
      throw Exception(e);
    }
  }

  Future<bool> postEvaluacionAlumno(EvaluacionAlumno evaluacionAlumno) async {
    try {
      await evaAlumnoCollec.add(evaluacionAlumno.toJSON());
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.postEvaluacionAlumno');
      throw Exception(e);
    }
  }

  Future<bool> deleteEvaluacionesAlumnoByEvaluacionId(String evaluacionId) async {
    try {
      QuerySnapshot querySnap = await evaAlumnoCollec
        .where('evaluacionId', isEqualTo: evaluacionId)
        .get();

      WriteBatch batch = FirebaseFirestore.instance.batch();

      querySnap.docs.forEach((doc) { 
        batch.delete(doc.reference);
      });

      await batch.commit();
      return true;
    } catch(e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.deleteEvaluacionesAlumnoByEvaluacionId');
      throw Exception(e);
    }
  }

  Future<bool> activateEvaluacionesAlumnoByEvaluacionId(String evaluacionId) async {
    try {
      QuerySnapshot querySnap = await evaAlumnoCollec
        .where('evaluacionId', isEqualTo: evaluacionId)
        .get();

      WriteBatch batch = FirebaseFirestore.instance.batch();

      querySnap.docs.forEach((doc) { 
        EvaluacionAlumno evaluacionAlumno = EvaluacionAlumno.fromJSON(doc.id, doc.data());
        evaluacionAlumno.activado = true;        
        batch.update(doc.reference, evaluacionAlumno.toJSON());
      });

      await batch.commit();
      return true;
    } catch(e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.activateEvaluacionesAlumnoByEvaluacionId');
      throw Exception(e);
    }
  }

  Future<void> culminateEvaluacionesAlumno(String id) async {
    try{      
      await evaAlumnoCollec.doc(id).update({'estado': 'terminado'});
    } catch(e) {
      dev.log(e.toString(), name: 'evaluacionAlumnoService.culminateEvaluacionesAlumno');
      throw Exception(e);
    }
  }
}