import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_games/models/Institucion.dart';
import 'dart:developer' as dev;

class InstitucionService {
  CollectionReference institucionCollection =
      FirebaseFirestore.instance.collection(Institucion.collectionId);

  Future<List<Institucion>> getInstituciones() async {
    try {
      QuerySnapshot querySnapshot = await institucionCollection.get();
      List<Institucion> instituciones = querySnapshot.docs
          .map((doc) => Institucion.fromJSON(doc.id, doc.data())).toList();
      return instituciones;
    } catch (e) {
      dev.log(e.toString(), name: 'institucionService.get');
      throw Exception(e);      
    }
  }
}
