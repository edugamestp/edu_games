import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer' as dev;
import 'package:edu_games/models/Habilidad.dart';

class HabilidadService {
  CollectionReference habilidadCollec =
      FirebaseFirestore.instance.collection(Habilidad.collectionId);

  Future<List<Habilidad>> getHabilidades(String alumnoId) async {
    try {
      QuerySnapshot querySnap = await habilidadCollec
          .where('alumnoId', isEqualTo: alumnoId)
          .get();

      List<Habilidad> habilidades = querySnap.docs
          .map((doc) => Habilidad.fromJSON(doc.id, doc.data()))
          .toList();
          
      return habilidades;
    } catch (e) {
      dev.log(e.toString(), name: 'habilidadService.getHabilidades');
      throw Exception(e);
    }
  }
}
