import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_games/models/Usuario.dart';
import 'dart:developer' as dev;

class UsuarioService {
  CollectionReference usuariosCollec =
      FirebaseFirestore.instance.collection(Usuario.collectionId);

  Future<Usuario> getUsuarioById(String id) async {
    try{
      DocumentSnapshot docSnap = await usuariosCollec.doc(id).get();
      Usuario usuario = Usuario.fromJSON(docSnap.id, docSnap.data());
      return usuario;
    } catch(e) {
      dev.log(e.toString(), name: 'usuarioService.getUsuarioById');
      throw Exception(e);
    }
  }

  Future<Usuario> getUsuarioAlumnoByDni(String dni) async {
    try {
      QuerySnapshot querySnap =
          await usuariosCollec.where('dni', isEqualTo: dni).get();
      if (querySnap.size == 0) return null;
      Usuario response = querySnap.docs
          .map((doc) => Usuario.fromJSON(doc.id, doc.data()))
          .toList()[0];
      //if (response.role != 'alumno' || response.grupoId.length > 0) return null;
      return response;
    } catch (e) {
      dev.log(e.toString(), name: 'usuarioService.getByDni');
      throw Exception(e);
    }
  }

  Future<List<Usuario>> getUsuariosByGrupoId(String grupoId) async {
    try {
      QuerySnapshot querySnap = await usuariosCollec
        .where('grupoId', isEqualTo: grupoId)
        .get();
      if(querySnap.size == 0) return null;
      List<Usuario> usuarios = querySnap.docs
        .map((doc) => Usuario.fromJSON(doc.id, doc.data()))
        .toList();
      return usuarios;
    } catch (e) {
      dev.log(e.toString(), name: 'usuarioService.getByGrupoId');
      throw Exception(e);
    }
  } 

  Future<bool> updateUsuario(Usuario usuario) async {
    try {
      await usuariosCollec.doc(usuario.id).set(usuario.toJSON());
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'usuarioService.updateUsuario');
      throw Exception(e);
    }
  }

  Future<bool> updateAlumnoGrupo(String grupoId, String nombre) async {
    try {
      QuerySnapshot querySnap =
          await usuariosCollec.where('grupoId', isEqualTo: grupoId).get();
      List<Usuario> alumnos = querySnap.docs.map((doc) => Usuario.fromJSON(doc.id, doc.data())).toList();
      Usuario foundAlumno = alumnos.where((alm) => alm.nombres + ' ' + alm.apellidos == nombre).toList()[0];
      foundAlumno.grupoId = '';
      await usuariosCollec.doc(foundAlumno.id).set(foundAlumno.toJSON());
      return true;
    } catch (e) {
      dev.log(e.toString(), name: 'usuarioService.updateAlumnoGrupo');
      throw Exception(e);
    }
  }

  Future<bool> updateAlumnosGrupo(String grupoId) async {
    try {
      QuerySnapshot querySnap = await usuariosCollec.where('grupoId', isEqualTo:  grupoId).get();
      WriteBatch batch = FirebaseFirestore.instance.batch();
      querySnap.docs.forEach((doc) {          
        Usuario usuario = Usuario.fromJSON(doc.id, doc.data());
        usuario.grupoId = '';
        batch.update(doc.reference, usuario.toJSON());
      });
      await batch.commit();
      return true;
    } catch(e) {
      dev.log(e.toString(), name: 'usuarioService.updateAlumnosGrupo');
      throw Exception(e);
    }
  }
}
