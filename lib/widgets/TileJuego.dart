import 'package:flutter/material.dart';

class TileJuego extends StatelessWidget {
  final String nombreJuego;
  final String imgUrl;
  final int duracion;
  final int numHabilidades;
  final Color color;

  TileJuego({
    this.nombreJuego,
    this.imgUrl,
    this.duracion,
    this.numHabilidades,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      elevation: 4,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 16.0,
          right: 16.0,
          left: 16.0,
          bottom: 8.0,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FadeInImage(
                  height: 48.0,
                  width: 48.0,
                  image: NetworkImage(imgUrl),
                  placeholder: AssetImage('images/load-buff.gif'),
                ),
                Text(
                  nombreJuego,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Icon(Icons.arrow_forward_ios),
              ],
            ),
            SizedBox(
              height: 8.0,
            ),
            Container(
              height: 36.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(Icons.timer_outlined),
                    Text('${duracion.toString().padLeft(2,"0")} minutos'),
                    Spacer(
                      flex: 1,
                    ),
                    Icon(Icons.bar_chart_rounded),
                    Text('${numHabilidades.toString().padLeft(2,"0")} habilidades'),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
