import 'package:flutter/material.dart';

class ProfileLabel extends StatelessWidget {
  final String imgUrl;
  final String nombres;
  final int edad;

  ProfileLabel({this.imgUrl, this.nombres, this.edad});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 32.0,
            backgroundImage: AssetImage(imgUrl),
          ),
          SizedBox(
            width: 12.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(nombres,
                  style:
                      TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
              Text('Edad $edad años'),
            ],
          )
        ],
      ),
    );
  }
}
