import 'package:flutter/material.dart';

class TileEvaluationProgress extends StatelessWidget {
  final String evaluacion;
  final int alumnos;

  TileEvaluationProgress({this.evaluacion, this.alumnos});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            Text('$evaluacion'),
            Expanded(child: SizedBox()),
            Text(
              '${alumnos.toString().padLeft(2, "0")} alumnos',
            ),
          ],
        ),
      ),
    );
  }
}
