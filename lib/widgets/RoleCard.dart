import 'package:flutter/material.dart';

class RoleCard extends StatelessWidget {
  final String imgPath;
  final String label;
  final Color color;

  RoleCard({
    @required this.imgPath,
    @required this.label,
    @required this.color
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        width: 110,
        height: 110,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(imgPath, width: 64.0, height: 64.0,),
            Text(
              label,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700
              ),
            )
          ],
        ),
      ),
    );
  }
}
