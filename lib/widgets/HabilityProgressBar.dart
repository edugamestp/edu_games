import 'package:flutter/material.dart';

class HabilityProgressBar extends StatelessWidget {
  final String label;
  final int value;
  final int maxValue;

  HabilityProgressBar({this.label, this.value, this.maxValue});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,        
        children: [
          Text(
            label,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  height: 15.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: LinearProgressIndicator(
                      value: (value / maxValue),
                      valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
                      backgroundColor: Colors.blueGrey[200],
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 24.0,
              ),
              Text(
                value.toString(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),              
            ],
          ),
        ],
      ),
    );
  }
}
