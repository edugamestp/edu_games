import 'package:edu_games/utils/constants.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final String text;
  final Color primaryColor;
  final Color contrastColor;
  final double minWidth;
  final double minHeight;
  final double fontSize;
  final GestureTapCallback onPressedAction;

  ActionButton(
      {@required this.text,
      this.primaryColor = kPrimaryColor,
      this.contrastColor = Colors.white,
      this.minWidth = 180,
      this.minHeight = 36,
      this.fontSize = 14,
      @required this.onPressedAction});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 4,
        primary: primaryColor,
        onPrimary: contrastColor,
        shape: StadiumBorder(),
        minimumSize: Size(minWidth, minHeight),
      ),
      onPressed: onPressedAction,
      child: Text(
        text,
        style: TextStyle(fontSize: fontSize),
      ),
    );
  }
}
