import 'package:flutter/material.dart';

class TileEvaluation extends StatelessWidget {
  final String evaluacion;
  final String estado;
  final String juego;
  final String role;
  final GestureTapCallback onPressedAction;

  TileEvaluation({
    this.evaluacion,
    this.estado,
    this.juego,
    this.role,
    this.onPressedAction,
  });

  final Map<String, String> estadosLabel = {
    'logrado': 'Concluído',
    'proceso': 'En proceso',
    'terminado': 'Concluído',
  };

  final Map<String, Color> estadosColor = {
    'logrado': Colors.green,
    'proceso': Colors.orange[300],
    'terminado': Colors.green,
  };

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (role == 'alumno' && estado == 'proceso') ? onPressedAction : (){},
      child: Container(
        height: 36.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.blueGrey[200],
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 4.0),
          child: Row(
            children: [
              Text('$evaluacion'),
              Expanded(child: SizedBox()),
              Material(
                borderRadius: BorderRadius.circular(20.0),
                elevation: 4,
                child: Container(
                  height: 28.0,
                  width: 90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: estadosColor[estado],
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      estadosLabel[estado],
                      style: TextStyle(color: Colors.white, fontSize: 10.0),
                    ),
                  ),
                ),
              ),
              /* if (role == 'alumno' && estado == 'proceso')
                IconButton(
                  tooltip: 'Jugar',
                  icon: Icon(CupertinoIcons.game_controller_solid),
                  onPressed: onPressedAction,
                ), */
              if (role == 'docente')
                IconButton(
                  tooltip: 'Opciones',
                  icon: Icon(Icons.more_horiz),
                  onPressed: onPressedAction,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
