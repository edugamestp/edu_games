import 'package:flutter/material.dart';

class TileStudent extends StatelessWidget {
  final String nombre;
  final GestureTapCallback onPressedAction;

  TileStudent({this.nombre, this.onPressedAction});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 4.0),
        child: Row(
          children: [
            Text('$nombre'),
            Expanded(child: SizedBox()),            
            IconButton(
              icon: Icon(Icons.close_rounded),
              onPressed: onPressedAction,
            )
          ],
        ),
      ),
    );
  }
}
