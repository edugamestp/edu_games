import 'package:edu_games/utils/constants.dart';
import 'package:flutter/material.dart';

class OutlineActionButton extends StatelessWidget {
  final String text;
  final Color color;
  final double minWidth;
  final double minHeight;
  final double fontSize;
  final GestureTapCallback onPressedAction;

  OutlineActionButton(
      {@required this.text,
      this.color = kSecondaryColor,
      this.minWidth = 50,
      this.minHeight = 20,
      this.fontSize = 12,
      @required this.onPressedAction});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: OutlinedButton.styleFrom(
        primary: color,
        shape: StadiumBorder(
          side: BorderSide(color: color),
        ),
        minimumSize: Size(minWidth, minHeight),
      ),
      onPressed: onPressedAction,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(
          text,
          style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
