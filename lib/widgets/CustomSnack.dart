import 'package:flutter/material.dart';

Widget customSnack({IconData icon, String message}) {
  return SnackBar(
    backgroundColor: Colors.blueGrey[600],
    shape: StadiumBorder(),
    content: Container(
      child: Row(
        children: [
          Icon(
            icon,
            color: Colors.white70,
          ),
          SizedBox(
            width: 20.0,
          ),
          Expanded(child: Text(message)),
        ],
      ),
    ),
    behavior: SnackBarBehavior.floating,
    duration: Duration(milliseconds: 1500),
  );
}