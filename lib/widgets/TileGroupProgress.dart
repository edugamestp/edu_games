import 'package:flutter/material.dart';

class TileGroupProgress extends StatelessWidget {
  final String grupo;
  final int totalAlumnos;
  final int actualAlumnos;  

  TileGroupProgress({
    this.grupo,
    this.totalAlumnos,
    this.actualAlumnos,    
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            Text('$grupo'),
            Expanded(child: SizedBox()),
            Text(
                '${actualAlumnos.toString().padLeft(2, '0')}/${totalAlumnos.toString().padLeft(2, '0')}'),
          ],
        ),
      ),
    );
  }
}
