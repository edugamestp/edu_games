import 'package:flutter/material.dart';

class TileStudentProgress extends StatelessWidget {
  final String nombre;

  TileStudentProgress({this.nombre});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            Text('$nombre'),
            Expanded(child: SizedBox()),
            Material(
              borderRadius:  BorderRadius.circular(20),
              elevation: 4,
              child: Container(
                alignment: Alignment.center,              
                height: 28.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.blueGrey[400],
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text('Ver progreso', style: TextStyle(color: Colors.white, fontSize: 10.0),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
