import 'package:flutter/material.dart';

class TileGrupo extends StatelessWidget {
  final String grupo;
  final String aula;
  final int totalAlumnos;
  final int actualAlumnos;
  final GestureTapCallback onPressedAction;

  TileGrupo({
    this.grupo,
    this.aula,
    this.totalAlumnos,
    this.actualAlumnos,
    this.onPressedAction,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 4.0),
        child: Row(
          children: [
            Text('$grupo($aula)'),
            Expanded(child: SizedBox()),
            Text(
                '${actualAlumnos.toString().padLeft(2, '0')}/${totalAlumnos.toString().padLeft(2, '0')}'),
            IconButton(
              icon: Icon(Icons.more_horiz),
              onPressed: onPressedAction,
            )
          ],
        ),
      ),
    );
  }
}
