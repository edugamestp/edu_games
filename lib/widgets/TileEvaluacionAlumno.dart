import 'package:edu_games/widgets/ActionButton.dart';
import 'package:flutter/material.dart';

class TileEvaluacionAlumno extends StatelessWidget {
  final String nombreEvaluacion;
  final String estado;

  TileEvaluacionAlumno({this.nombreEvaluacion, this.estado});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.blueGrey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 4.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Text('$nombreEvaluacion'),
            Expanded(child: SizedBox()),
            ActionButton(
              text: estado == 'logrado' ? 'Objetivo alcanzado' : 'En Proceso',
              primaryColor:
                  estado == 'logrado' ? Colors.green : Colors.orange[300],
              minWidth: 40.0,
              minHeight: 26.0,
              fontSize: 10,
              onPressedAction: () {},
            )
          ],
        ),
      ),
    );
  }
}
