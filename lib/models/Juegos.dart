class Juego {
  String id;
  String nombre;
  String descripcion;
  int duracion;
  String imgUrl;
  List<String> habilidades;

  static const String collectionId = 'juegos';

  Juego({
    this.id,
    this.nombre,
    this.descripcion,
    this.duracion,
    this.imgUrl,
    this.habilidades,
  });

  Juego.fromJSON(String idDoc, Map<String, dynamic> juego) {
    id = idDoc;
    nombre = juego['nombre'];
    descripcion = juego['descripcion'];
    duracion = juego['duracion'];
    imgUrl = juego['imgUrl'];
    habilidades = juego['habilidades'].cast<String>();
  }

  Map<String, dynamic> toJSON() => {
    'nombre': nombre,
    'descripcion': descripcion,
    'duracion': duracion,
    'imgUrl': imgUrl,
    'habilidades': habilidades,
  };
}
