class Pregunta {
  String id;
  String objeto;
  String pregunta;
  int rptaCorrecta = 0;
  List<String> imagenRespuesta;

  static const String collectionId = 'preguntas';

  Pregunta({
    this.objeto,
    this.pregunta,
    this.rptaCorrecta,
    this.imagenRespuesta,
  });

  Pregunta.fromJSON(String idDoc, Map<String, dynamic> pgt) {
    id = idDoc;
    objeto = pgt['objeto'];
    pregunta = pgt['pregunta'];
    rptaCorrecta = 0;
    imagenRespuesta = pgt['imagenRespuesta'].cast<String>();
  }

  @override
    String toString() {
      return 'Pregunta {objeto: $objeto, pregunta: $pregunta, rptaCorrecta: $rptaCorrecta';
    }
}
