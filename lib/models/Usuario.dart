import 'package:cloud_firestore/cloud_firestore.dart';

class Usuario {
  String id;
  String nombres;
  String apellidos;
  String dni;
  String genero;
  DateTime fechaNacimiento;
  String telefono;
  String institucion;
  String correo;
  String contrasenya;
  String contrasenyaKey;
  String grupoId;
  String role;

  static const String collectionId = 'usuarios';

  Usuario(
      {this.id,
      this.nombres,
      this.apellidos,
      this.dni,
      this.genero,
      this.fechaNacimiento,
      this.telefono,
      this.institucion,
      this.correo,
      this.contrasenya,
      this.contrasenyaKey,
      this.grupoId = '',
      this.role});

  Usuario.fromJSON(String idDoc, Map<String, dynamic> usuario) {
    id = idDoc;
    nombres = usuario['nombres'];
    apellidos = usuario['apellidos'];
    dni = usuario['dni'];
    genero = usuario['genero'];
    Timestamp t = usuario['fechaNacimiento'];
    fechaNacimiento = t.toDate();
    telefono = usuario['telefono'];
    institucion = usuario['institucion'];
    correo = usuario['correo'];
    contrasenya = usuario['contrasenya'];
    contrasenyaKey = usuario['contrasenyaKey'];
    grupoId = usuario['grupoId'];
    role = usuario['role'];
  }

  Map<String, dynamic> toJSON() => {
        'nombres': nombres,
        'apellidos': apellidos,
        'dni': dni,
        'genero': genero,
        'fechaNacimiento': fechaNacimiento,
        'telefono': telefono,
        'institucion': institucion,
        'correo': correo,
        'contrasenya': contrasenya,
        'contrasenyaKey': contrasenyaKey,
        'grupoId': grupoId,
        'role': role
      };

  @override
  String toString() {
    return 'Usuario{id: $id, nombres: $nombres, apellidos: $apellidos, dni: $dni, genero: $genero, fechaNacimiento: $fechaNacimiento, telefono: $telefono, institucion: $institucion, correo: $correo, contrasenya: $contrasenya, contrasenyaKey: $contrasenyaKey, grupoId: $grupoId, role: $role }';
  }
}
