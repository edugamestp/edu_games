class EvaluacionAlumno {
  String id;
  String alumnoId;
  String evaluacionId;
  String nombreEvaluacion;
  String estado;
  bool activado;
  int puntaje;
  String juegoId;

  static const String collectionId = 'evaluacion_alumno';

  EvaluacionAlumno({
    this.id,
    this.alumnoId,
    this.evaluacionId,
    this.nombreEvaluacion,
    this.estado = 'proceso',
    this.activado = false,
    this.puntaje = 0,
    this.juegoId,
  });

  EvaluacionAlumno.fromJSON(String idDoc, Map<String, dynamic> evaluacionAlumno) {
    id = idDoc;
    alumnoId = evaluacionAlumno['alumnoId'];
    evaluacionId = evaluacionAlumno['evaluacionId'];
    nombreEvaluacion = evaluacionAlumno['nombreEvaluacion'];
    estado = evaluacionAlumno['estado'];
    activado = evaluacionAlumno['activado'];
    puntaje = evaluacionAlumno['puntaje'];
    juegoId = evaluacionAlumno['juegoId'];
  }

  Map<String, dynamic> toJSON() => {        
        'alumnoId': alumnoId,
        'evaluacionId': evaluacionId,
        'nombreEvaluacion': nombreEvaluacion,
        'estado': estado,
        'activado': activado,
        'puntaje': puntaje,
        'juegoId': juegoId,
      };

  @override
  String toString() {
    return 'EvaluacionAlumno {id: $id, alumnoId: $alumnoId, evaluacionId: $evaluacionId, nombreEvaluacion: $nombreEvaluacion, estado: $estado, puntaje: $puntaje}';
  }
}
