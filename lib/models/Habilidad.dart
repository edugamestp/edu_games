class Habilidad {
  String id;
  String habCognitiva;
  String alumnoId;
  int puntaje;
  int maxPuntaje;

  static const String collectionId = 'habilidades';

  Habilidad({
    this.id,
    this.habCognitiva,
    this.alumnoId,
    this.puntaje,
    this.maxPuntaje = 5,
  });

  Habilidad.fromJSON(String idDoc, Map<String, dynamic> habilidad) {
    id = idDoc;
    habCognitiva = habilidad['habCognitiva'];
    alumnoId = habilidad['alumnoId'];
    puntaje = habilidad['puntaje'];
    maxPuntaje = habilidad['maxPuntaje'];
  }

  Map<String, dynamic> toJSON() => {
        'habCognitiva': habCognitiva,
        'alumnoId': alumnoId,
        'puntaje': puntaje,
        'maxPuntaje': maxPuntaje,
      };

  @override
  String toString() {
    return 'Habilidad {id: $id, habCognitiva: $habCognitiva, alumnoId: $alumnoId, puntaje: $puntaje, maxPuntaje: $maxPuntaje}';
  }
}
