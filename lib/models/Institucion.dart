class Institucion {
  String id;
  String nombre;
  String tipo;

  static const String collectionId = 'instituciones';

  Institucion({this.id, this.nombre, this.tipo});

  Institucion.fromJSON(String idDoc, Map<String, dynamic> institucion) {
    id = idDoc;
    nombre = institucion['nombre'];
    tipo = institucion['tipo'];
  }

  Map<String, dynamic> toJSON() => {'nombre': nombre, 'tipo': tipo};

  @override
  String toString() {
    return 'Institucion{id: $id, nombre: $nombre, tipo: $tipo}';
  }
}
