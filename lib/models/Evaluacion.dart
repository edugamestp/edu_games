import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Evaluacion {
  String id;
  String nombre;
  String estado;
  bool activado;
  String hora;
  String dia;
  DateTime fechaISO;
  TimeOfDay timeISO;
  String docenteId;
  String grupoId;
  String juegoId;
  int alumnos;

  static const String collectionId = 'evaluaciones';

  Evaluacion({
    this.id,
    this.nombre,
    this.estado = 'proceso',
    this.activado = false,
    this.hora,
    this.dia,
    this.fechaISO,
    this.timeISO,
    this.docenteId,
    this.grupoId,
    this.juegoId,
    this.alumnos,
  });

  Evaluacion.fromJSON(String idDoc, Map<String, dynamic> evaluacion) {
    id = idDoc;
    nombre = evaluacion['nombre'];
    estado = evaluacion['estado'];
    activado = evaluacion['activado'];
    hora = evaluacion['hora'];
    dia = evaluacion['dia'];
    Timestamp f = evaluacion['fechaISO'];
    fechaISO = f.toDate();
    Timestamp t = evaluacion['timeISO'];
    timeISO = TimeOfDay.fromDateTime(t.toDate());
    docenteId = evaluacion['docenteId'];
    grupoId = evaluacion['grupoId'];
    juegoId = evaluacion['juegoId'];
    alumnos = evaluacion['alumnos'];
  }

  Map<String, dynamic> toJSON() {
    final now = DateTime.now();
    final formatTimeISO = DateTime(now.year, now.month, now.day, timeISO.hour, timeISO.minute);
    return {
      'nombre': nombre,
      'estado': estado,
      'activado': activado,
      'hora': hora,
      'dia': dia,
      'fechaISO': fechaISO,
      'timeISO': formatTimeISO,
      'docenteId': docenteId,
      'grupoId': grupoId,
      'juegoId': juegoId,
      'alumnos': alumnos,
    };
  }

  @override
  String toString() {
    return 'Evaluacion {id: $id, nombre: $nombre, estado: $estado, hora: $hora, dia: $dia, timeISO: $timeISO, docenteId: $docenteId, grupoId: $grupoId, juegoId: $juegoId';
  }
}
