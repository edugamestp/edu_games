class Grupo {
  String id;
  String nombre;
  String aula;
  int totalAlumnos;
  int actualAlumnos;
  String idDocente;
  String dniDocente;
  List<String> alumnos;

  static const String collectionId = 'grupos';

  Grupo({
    this.id,
    this.nombre,
    this.aula,
    this.totalAlumnos = 15,
    this.actualAlumnos = 0,
    this.idDocente,
    this.dniDocente,
    this.alumnos = const [],
  });

  Grupo.fromJSON(String idDoc, Map<String, dynamic> grupo) {
    id = idDoc;
    nombre = grupo['nombre'];
    aula = grupo['aula'];
    idDocente = grupo['idDocente'];
    dniDocente = grupo['dniDocente'];
    totalAlumnos = grupo['totalAlumnos'];
    actualAlumnos = grupo['actualAlumnos'];
    alumnos = grupo['alumnos'].cast<String>();
  }

  Map<String, dynamic> toJSON() => {
        'nombre': nombre,
        'aula': aula,
        'idDocente': idDocente,
        'dniDocente': dniDocente,
        'totalAlumnos': totalAlumnos,
        'actualAlumnos': actualAlumnos,
        'alumnos': alumnos,
      };

  @override
  String toString() {
    return 'Grupo {id: $id, nombre: $nombre, aula: $aula, idDocente: $idDocente, dniDocente: $dniDocente, totalAlumnos: $totalAlumnos, actualAlumnos: $actualAlumnos, alumnos: ${alumnos.toString()}}';
  }
}
