import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/services/usuario-service.dart';
import 'package:flutter/material.dart';

class UsuarioProvider with ChangeNotifier {
  final UsuarioService _usuarioService = UsuarioService();
  final SharedPreferenceService _prefs = SharedPreferenceService();
  Usuario _usuario;

  Usuario get usuario => _usuario;

  Future<Usuario> getUsuario() async {    
    final response = await _usuarioService.getUsuarioById(_prefs.userId);
    _usuario = response;
    notifyListeners();
    return response;
  }
  
}