import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/services/evaluacion-alumno-service.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:flutter/material.dart';

class EvaluacionAlumnoProvider with ChangeNotifier {
  final EvaluacionAlumnoService _evaluacionAlumnoService = EvaluacionAlumnoService();
  final SharedPreferenceService _prefs = SharedPreferenceService();
  List<EvaluacionAlumno> _evaluaciones = [];

  List<EvaluacionAlumno> get evaluaciones => _evaluaciones;

  Future<List<EvaluacionAlumno>> getEvaluacionesByAlumno() async {
    final response = await _evaluacionAlumnoService.getEvaluacionesByAlumno(_prefs.userId);
    _evaluaciones = response;
    notifyListeners();
    return response;
  }

  Future<List<EvaluacionAlumno>> getEvaluacionesActivasByAlumno() async {
    final response = await _evaluacionAlumnoService.getEvaluacionesActivasByAlumno(_prefs.userId);
    _evaluaciones = response;
    notifyListeners();
    return response;
  }

  culminateEvaluacionesAlumno(String id) async {
    await _evaluacionAlumnoService.culminateEvaluacionesAlumno(id);    
    notifyListeners();
  }
}