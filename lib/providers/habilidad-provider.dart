import 'package:edu_games/models/Habilidad.dart';
import 'package:edu_games/services/habilidad-service.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:flutter/material.dart';

class HabilidadProvider with ChangeNotifier{
  final HabilidadService _habilidadService = HabilidadService();
  final SharedPreferenceService _prefs = SharedPreferenceService();
  
  List<Habilidad> _habilidades = [];

  List<Habilidad> get habilidades => _habilidades;  

  Future<List<Habilidad>> getHabilidades() async {    
    final response = await _habilidadService.getHabilidades(_prefs.userId);   
    _habilidades = response;
    notifyListeners();
    return response;
  }

}