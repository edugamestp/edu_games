import 'package:edu_games/models/Juegos.dart';
import 'package:edu_games/services/juego-service.dart';
import 'package:flutter/material.dart';

class JuegoProvider with ChangeNotifier {
  final JuegoService _juegoService = JuegoService();
  
  List<Juego> _juegos = [];

  List<Juego> get juegos => _juegos;  

  JuegoProvider() {
    getJuegos();
  }

  Future<List<Juego>> getJuegos() async {
    final response = await _juegoService.getJuegos();
    _juegos = response;
    notifyListeners();
    return response;
  }  

  Future<List<Juego>> getQueryJuegos(List<String> searchKeys) async {
    List<Juego> response = await _juegoService.getJuegos();
    if(searchKeys != null && searchKeys.length > 0) {      
      response = response.where((jg) => jg.habilidades.any((e) => searchKeys.contains(e))).toList();
    }
    _juegos = response;
    notifyListeners();
    return response;
  }

  Juego getJuego(String id) {
    final juego = _juegos.where((jg) => jg.id == id).toList().first;    
    return juego;
  }
}