import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/services/grupo-service.dart';
import 'package:flutter/cupertino.dart';

class GrupoProvider with ChangeNotifier {
  final GrupoService _grupoService = GrupoService();
  bool _isloading = true;
  List<Grupo> _grupos = [];
  Grupo _selectedGrupo;

  GrupoProvider() {
    getGruposByDocente();
  }

  List<Grupo> get grupos => _grupos;
  bool get isLoading => _isloading;
  Grupo get selectedGrupo => _selectedGrupo;

  set selectedGrupo(Grupo value) {
    _selectedGrupo = value;
    notifyListeners();
  }

  Future<List<Grupo>> getGruposByDocente() async {
    _isloading = true;
    final response = await _grupoService.getGruposByDocente();
    _isloading = false;
    _grupos = response;
    notifyListeners();
    return response;
  }

  Future<String> addAlumnoToGrupo(Grupo grupo, String alumnoDni) async {
    _isloading = true;
    final response = await _grupoService.addAlumnoToGrupo(grupo, alumnoDni);
    _isloading = false;
    notifyListeners();
    return response;
  }

  createGrupo(Grupo grupo) async {
    _isloading = true;
    final response = await _grupoService.postGrupo(grupo);
    _isloading = false;
    notifyListeners();
    return response;
  }

  updateGrupo(Grupo grupo) async {
    _isloading = true;
    final response = await _grupoService.putGrupo(grupo);
    _isloading = false;
    notifyListeners();
    return response;
  }

  Future<bool> updateAlumnos(String alumno) async {
    _isloading = true;
    Grupo gp = _selectedGrupo;
    final response = await _grupoService.updateAlumnos(gp, alumno);
    _isloading = false;
    if (response != null) {
      _selectedGrupo = response;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteGrupo() async {
    _isloading = true;
    Grupo gp = _selectedGrupo;
    final response = await _grupoService.deleteGrupo(gp);
     _isloading = false;
    if (response) {
      _selectedGrupo = null;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }
}
