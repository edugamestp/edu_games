import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/services/evaluacion-service.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:flutter/material.dart';

class EvaluacionProvider with ChangeNotifier {
  final EvaluacionSevice _evaluacionSevice = EvaluacionSevice();
  final SharedPreferenceService _prefs = SharedPreferenceService();
  bool _isLoading = true;
  List<Evaluacion> _evaluaciones = [];

  List<Evaluacion> get evaluaciones => _evaluaciones;
  bool get isLoading => _isLoading;

  /* Future<List<Evaluacion>> getEvaluacionesByGrupo() async {
    _isLoading = true;
    final response = await _evaluacionSevice.getEvaluacionesByGrupo(_prefs.userGrupoId);
    _isLoading = false;
    _evaluaciones = response;
    notifyListeners();
    return response;
  } */

  Future<List<Evaluacion>> getQueryEvaluacionesByDocente(String searchKey) async {
    List<Evaluacion> response = await _evaluacionSevice.getEvaluacionesByDocenteId(_prefs.userId); 
    if(searchKey != null && searchKey.length > 0) {
      response = response.where((eva) => eva.nombre.contains(searchKey)).toList();      
    }
    _evaluaciones = response;      
    notifyListeners();
    return response;
  }


  Future<bool> createEvaluacion(Evaluacion evaluacion) async {
    evaluacion.docenteId = _prefs.userId;
    final response = await _evaluacionSevice.postEvaluacion(evaluacion);
    notifyListeners();
    return response;
  }

  Future<bool> updateEvaluacion(Evaluacion evaluacion) async {
    final response = await _evaluacionSevice.putEvaluacion(evaluacion);
    notifyListeners();
    return response;
  }

  Future<bool> deleteEvaluacion(Evaluacion evaluacion) async {
    final response = await _evaluacionSevice.deleteEvaluacion(evaluacion);
    notifyListeners();
    return response;
  }

  activateEvaluacion(Evaluacion evaluacion) async {
    final response = await _evaluacionSevice.activateEvaluacion(evaluacion);
    notifyListeners();
    return response;
  }
}
