import 'package:edu_games/providers/evaluacion-alumno-provider.dart';
import 'package:edu_games/providers/evaluacion-provider.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/providers/habilidad-provider.dart';
import 'package:edu_games/providers/juego-provider.dart';
import 'package:edu_games/providers/usuario-provider.dart';
import 'package:edu_games/screens/entrenamiento/TrainingPage.dart';
import 'package:edu_games/screens/evaluacion/CreateEvaluationPage.dart';
import 'package:edu_games/screens/evaluacion/EvaluationPage.dart';
import 'package:edu_games/screens/grupo/CreateGroupPage.dart';
import 'package:edu_games/screens/grupo/EditStudentsPage.dart';
import 'package:edu_games/screens/grupo/GroupPage.dart';
import 'package:edu_games/screens/juego/CuentaCosasGame.dart';
import 'package:edu_games/screens/juego/CuentaCosasPage.dart';
import 'package:edu_games/screens/juego/CuentaCosasSplash.dart';
import 'package:edu_games/screens/juego/TocaCirculosGame.dart';
import 'package:edu_games/screens/juego/TocaCirculosPage.dart';
import 'package:edu_games/screens/juego/TocaCirculosSplash.dart';
import 'package:edu_games/screens/login/LoginPage.dart';
import 'package:edu_games/screens/main/MainPage.dart';
import 'package:edu_games/screens/login/PreLoginPage.dart';
import 'package:edu_games/screens/perfil/ChangePasswordPage.dart';
import 'package:edu_games/screens/perfil/EditProfilePage.dart';
import 'package:edu_games/screens/perfil/ProfilePage.dart';
import 'package:edu_games/screens/progreso/GroupProgressPage.dart';
import 'package:edu_games/screens/progreso/ProgresoPage.dart';
import 'package:edu_games/screens/register/RegisterPage.dart';
import 'package:edu_games/screens/register/SelectRolePage.dart';
import 'package:edu_games/screens/welcome/WelcomePage.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final prefs = SharedPreferenceService();
  await prefs.initData();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GrupoProvider()),
        ChangeNotifierProvider(create: (_) => EvaluacionProvider()),
        ChangeNotifierProvider(create: (_) => HabilidadProvider()),
        ChangeNotifierProvider(create: (_) => UsuarioProvider()),
        ChangeNotifierProvider(create: (_) => EvaluacionAlumnoProvider()),
        ChangeNotifierProvider(create: (_) => JuegoProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Edu Games',
        theme: ThemeData(
          fontFamily: 'Nunito',
          primarySwatch: kPrimaryPalette,
          inputDecorationTheme: InputDecorationTheme(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
        ),
        initialRoute: WelcomePage.routName,
        routes: {
          WelcomePage.routName: (BuildContext context) => WelcomePage(),
          PreLoginPage.routName: (BuildContext context) => PreLoginPage(),
          LoginPage.routName: (BuildContext context) => LoginPage(),
          SelectRolePage.routName: (BuildContext context) => SelectRolePage(),
          RegisterPage.routName: (BuildContext context) => RegisterPage(),
          MainPage.routName: (BuildContext context) => MainPage(),
          ProgresoPage.routName: (BuildContext context) => ProgresoPage(),
          GroupProgressPage.routName: (BuildContext context) => GroupProgressPage(),
          GroupPage.routName: (BuildContext context) => GroupPage(),
          CreateGroupPage.routName: (BuildContext context) => CreateGroupPage(),
          EditStudentsPage.routName: (BuildContext context) => EditStudentsPage(),
          EvaluationPage.routName: (BuildContext context) => EvaluationPage(),
          CreateGroupPage.routName: (BuildContext context) => CreateEvaluationPage(),
          TrainingPage.routName: (BuildContext context) => TrainingPage(),
          ProfilePage.routName: (BuildContext context) => ProfilePage(),
          EditProfilePage.routName: (BuildContext context) => EditProfilePage(),
          ChangePasswordPage.routName: (BuildContext context) => ChangePasswordPage(),
          TocaCirculosPage.routName: (BuildContext context) => TocaCirculosPage(),
          TocaCirculosSplash.routName: (BuildContext context) => TocaCirculosSplash(),
          TocaCirculosGame.routName: (BuildContext context) => TocaCirculosGame(),
          CuentaCosasPage.routName: (BuildContext context) => CuentaCosasPage(),
          CuentaCosasSplash.routName: (BuildContext context) => CuentaCosasSplash(),
          CuentaCosasGame.routName: (BuildContext context) => CuentaCosasGame(),
        },
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('es', 'ES'),
        ],
      ),
    );
  }
}
