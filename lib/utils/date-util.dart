import 'package:flutter/material.dart';

String formatDateUtil(DateTime date) {
  return '${date.day.toString().padLeft(2, '0')}/${date.month.toString().padLeft(2, '0')}/${date.year}';
}

String formatTimeUtil(TimeOfDay time) {
  int hour = time.hour;
  int minute = time.minute;
  String meridiem;
  if (hour > 11) {
    meridiem = 'PM';
    hour = hour != 12 ? (hour - 12) : 12;
  } else {
    meridiem = 'AM';
  }
  return '${hour.toString().padLeft(2, '0')} : ${minute.toString().padLeft(2, '0')} $meridiem';
}


const Map<int, String> mapWeekDay = {
  1: 'Lunes',
  2: 'Martes',
  3: 'Miércoles',
  4: 'Jueves',
  5: 'Viernes',
  6: 'Sábado',
  7: 'Domingo',
};