import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFF47C48);
const kSecondaryColor = Color(0xFFF7B194);
const kTertiaryColor = Colors.white;

const _primaryColor = 0xFFF47C48;
const MaterialColor kPrimaryPalette = MaterialColor(_primaryColor, <int, Color>{
  50: Color(0xFFFEEFE9),
  100: Color(0xFFFCD8C8),
  200: Color(0xFFFABEA4),
  300: Color(0xFFF7A37F),
  400: Color(0xFFF69063),
  500: Color(_primaryColor),
  600: Color(0xFFF37441),
  700: Color(0xFFF16938),
  800: Color(0xFFEF5F30),
  900: Color(0xFFEC4C21),
});

final Map<int, Color> kColorsTile = {
  1: Colors.red[200],
  2: Colors.blue[200],
  3: Colors.yellow[300],
};