
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

class ScaffoldUtil with ChangeNotifier {
  ScaffoldMessengerState _scaffold;

  ScaffoldUtil(this._scaffold);

  ScaffoldMessengerState get scaffold => _scaffold;
}