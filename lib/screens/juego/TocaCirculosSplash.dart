import 'package:edu_games/screens/juego/TocaCirculosGame.dart';
import 'package:flutter/material.dart';

class TocaCirculosSplash extends StatefulWidget {
  static final String routName = 'splash-tocacirculos';

  @override
  _TocaCirculosSplashState createState() => _TocaCirculosSplashState();
}

class _TocaCirculosSplashState extends State<TocaCirculosSplash> {
  @override
  void initState() {
    super.initState();
    delayedTransition();
  }

  Future<void> delayedTransition() async {
    Future.delayed(Duration(seconds: 3), () => animatedTransition());
  }

  animatedTransition() {
    final args = ModalRoute.of(context)?.settings.arguments as Map<String, String>;
    final evaAlumnoId = args != null ? args['evaAlumnoId'] : '';

    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secAnimation) {
          return TocaCirculosGame(evaAlumnoId: evaAlumnoId,);
        },
        transitionDuration: Duration(seconds: 1),
        transitionsBuilder: (context, animation, secAnimation, child) {
          animation =
              CurvedAnimation(parent: animation, curve: Curves.easeInOut);
          return FadeTransition(
            opacity: Tween<double>(begin: 0.0, end: 1.0).animate(animation),
            child: child,
          );
        }));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/jg-001-background.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Text(
              'Toca\nCírculos!',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 48.0, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}
