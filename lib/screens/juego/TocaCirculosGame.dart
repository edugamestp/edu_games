import 'dart:async';
import 'dart:math';
import 'package:edu_games/providers/evaluacion-alumno-provider.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:provider/provider.dart';
import 'package:vector_math/vector_math.dart' as vc;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TocaCirculosGame extends StatefulWidget {
  static final String routName = 'play-tocacirculos';
  final String evaAlumnoId;

  TocaCirculosGame({this.evaAlumnoId});

  @override
  _TocaCirculosGameState createState() => _TocaCirculosGameState();
}

class _TocaCirculosGameState extends State<TocaCirculosGame> {
  String _textPC = 'Turno del\nOrdenador';
  String _textPlayer = 'Tu Turno';
  double _fontSizePC = 24.0;
  double _fontSizePlayer = 24.0;
  List<int> _guessNumbers = [];
  List<int> _guessedNumbers = [];
  int _difficulty = 2;
  int _maxDifficulty = 4;
  int _maxAttemps = 2;
  double radio = 160.0;
  double _angle = -36.0;
  int _selectedNumber = 99;
  int _attemps;
  bool _turnPC;
  bool _turnPlayer;
  double _percent = 0.0;
  bool _canReturn = false;
  final Duration _tick = Duration(seconds: 1);
  int _secondsPassed = 0;
  bool _isActive = false;
  Timer _timer;  

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    playGame(difficulty: _difficulty);
  }

  @override
  dispose() {
    _timer.cancel();
    _timer = null;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  Future<bool> popToPreviewPage() async {
    if (_canReturn) {
      Navigator.pop(context);
    }
  }

  Future<void> playGame({int difficulty}) async {
    setState(() {
      _turnPC = true;
      _turnPlayer = false;
      _textPC = 'Turno del\nOrdenador';
      _textPlayer = 'Tu Turno';
      _fontSizePC = 24.0;
      _fontSizePlayer = 24.0;
      _guessNumbers = [];
      _guessedNumbers = [];
      _selectedNumber = 99;
      _attemps = 0;
      _maxAttemps = difficulty;
      _canReturn = false;
    });

    initNumbers();

    await Future.delayed(Duration(seconds: 2), () {});

    await playOrdenador();

    setState(() {
      _turnPC = false;
      _turnPlayer = true;
      _canReturn = true;
      _isActive = true;
    });

    startTimer();
  }

  initNumbers() {
    Random rdm = Random();
    for (int i = 0; i < _difficulty; i++) {
      int rdmNumber = rdm.nextInt(10);
      if (_guessNumbers.contains(rdmNumber)) rdmNumber = rdm.nextInt(10);
      _guessNumbers.add(rdmNumber);
    }
  }

  playOrdenador() async {
    for (int i = 0; i < _guessNumbers.length; i++) {
      await Future.delayed(Duration(seconds: 1), () {
        setState(() {
          _textPC = _guessNumbers[i].toString();
          _fontSizePC = 54.0;
        });
      });

      await Future.delayed(Duration(seconds: 2), () {
        setState(() {
          _textPC = ' ';
        });
      });
    }
  }

  selectNumber(int value, BuildContext context) async {
    setState(() {
      _selectedNumber = value;
      _guessedNumbers.add(value);
      _attemps += 1;
      if (_attemps == _maxAttemps) _isActive = false;
    });

    await Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _selectedNumber = 99;
      });
    });

    if (_attemps == _maxAttemps) {
      Future.delayed(Duration(seconds: 2), () {
        setState(() {
          _difficulty += 1;
        });

        if (_difficulty <= _maxDifficulty) {
          playGame(difficulty: _difficulty);
        } else {
          return showEndDialog().then((value) => Navigator.pop(context));
        }
      });
    }
  }

  handleTick() {
    if (_isActive) {
      setState(() {
        _secondsPassed += 1;
      });
    }
  }

  startTimer() {
    if (_timer == null) {
      _timer = Timer.periodic(_tick, (Timer t) {
        handleTick();
      });
    }
  }

  Widget evaluatePlayer() {
    int rightguess = 0;
    for (int i = 0; i < _guessNumbers.length; i++) {
      if (_guessNumbers[i] == _guessedNumbers[i]) rightguess++;
    }
    _percent = rightguess / _guessNumbers.length;
    return AnimatedProgressIndicator(value: _percent);
  }

  Future showEndDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: Container(
            height: 250,
            width: 400,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0), color: Colors.white),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '¡Juego Terminado!',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 24.0,
                      ),
                      ActionButton(
                        text: 'Salir',
                        fontSize: 16.0,
                        onPressedAction: () {
                          final evaAlumnoProvider = Provider.of<EvaluacionAlumnoProvider>(context,listen: false);
                          evaAlumnoProvider.culminateEvaluacionesAlumno(widget.evaAlumnoId);
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 45.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(45.0)),
                      child: Image.asset('images/congrats.png'),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: RotatedBox(
                    quarterTurns: 3,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 45.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(45.0)),
                        child: Image.asset('images/congrats.png'),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;    

    return WillPopScope(
      onWillPop: popToPreviewPage,
      child: Scaffold(
        body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.green[300], Colors.yellow[300]],
                stops: [0, 1],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
              ),
            ),
            child: Stack(
              children: [
                if (_turnPC)
                  BigCircle(
                    mediaSize: mediaSize,
                    text: _textPC,
                    fontSize: _fontSizePC,
                  ),
                if (_turnPlayer)
                  BigCircle(
                    mediaSize: mediaSize,
                    text: _textPlayer,
                    fontSize: _fontSizePlayer,
                  ),
                if (_turnPlayer)
                  Align(
                    alignment: Alignment(0, 0),
                    child: Container(
                      width: mediaSize.width * 0.42,
                      height: mediaSize.width * 0.42,
                      child: Stack(
                        children: [
                          ...List.generate(10, (index) {
                            _angle += 36.0;
                            final posX = double.parse(
                                sin(vc.radians(_angle)).toStringAsFixed(2));
                            final posY = double.parse(cos(vc.radians(_angle))
                                    .toStringAsFixed(2)) *
                                -1;
                            return SmallCircle(
                              posX: posX,
                              posY: posY,
                              mediaSize: mediaSize,
                              value: index,
                              color: _selectedNumber == index
                                  ? Colors.blue[300]
                                  : Colors.white,
                              onSelect: _attemps != _maxAttemps
                                  ? () => selectNumber(index, context)
                                  : null,
                            );
                          }),
                        ],
                      ),
                    ),
                  ),
                Align(
                  alignment: Alignment(0.8, -0.85),
                  child: Material(
                    elevation: 4,
                    borderRadius: BorderRadius.circular(5.0),
                    child: Container(
                      alignment: Alignment.center,
                      width: mediaSize.width * 0.07,
                      height: mediaSize.height * 0.07,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: Colors.green[100],
                      ),
                      child: Text(
                        '${(_secondsPassed ~/ 60).toString().padLeft(2, '0')}:${(_secondsPassed % 60).toString().padLeft(2, '0')}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0.8, 0.3),
                  child: Material(
                    elevation: 4,
                    borderRadius: BorderRadius.circular(5.0),
                    child: Container(
                      width: mediaSize.width * 0.07,
                      height: mediaSize.height * 0.7,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: RotatedBox(
                          quarterTurns: -1,
                          child: _attemps == _maxAttemps
                              ? evaluatePlayer()
                              : LinearProgressIndicator(
                                  value: 0,
                                  backgroundColor: Colors.white,
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
                if (_attemps == _maxAttemps)
                  Align(
                    alignment: Alignment(0.8, 0.1),
                    child: Container(
                      alignment: Alignment.center,
                      width: mediaSize.width * 0.07,
                      height: mediaSize.height * 0.1,
                      child: Text(
                        (_percent * 100).toStringAsFixed(0) + '%',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SmallCircle extends StatelessWidget {
  final double posX;
  final double posY;
  final Size mediaSize;
  final int value;
  final Color color;
  final GestureTapCallback onSelect;

  SmallCircle({
    this.posX,
    this.posY,
    this.mediaSize,
    this.value,
    this.color,
    this.onSelect,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(posX, posY),
      child: InkWell(
        onTap: onSelect,
        child: Container(
          width: mediaSize.width * 0.06,
          height: mediaSize.width * 0.06,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 10,
                offset: Offset(0, 4),
              )
            ],
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              value.toString(),
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}

class BigCircle extends StatelessWidget {
  final Size mediaSize;
  final String text;
  final double fontSize;

  BigCircle({this.mediaSize, this.text, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, 0),
      child: Container(
        width: mediaSize.width * 0.25,
        height: mediaSize.width * 0.25,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 10,
              offset: Offset(0, 4),
            )
          ],
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: fontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

class AnimatedProgressIndicator extends StatefulWidget {
  final double value;

  AnimatedProgressIndicator({this.value});

  @override
  _AnimatedProgressIndicatorState createState() =>
      new _AnimatedProgressIndicatorState();
}

class _AnimatedProgressIndicatorState extends State<AnimatedProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1500), vsync: this);
    animation = Tween(begin: 0.0, end: widget.value).animate(controller)
      ..addListener(() {
        setState(() {});
      });
    controller.forward();
  }

  @override
  void dispose() {
    controller.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: animation.value,
      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue[400]),
      backgroundColor: Colors.white,
    );
  }
}