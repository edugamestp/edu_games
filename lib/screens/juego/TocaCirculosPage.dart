import 'package:edu_games/providers/juego-provider.dart';
import 'package:edu_games/screens/juego/TocaCirculosSplash.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TocaCirculosPage extends StatelessWidget {
  static final String routName = 'jg-001';

  @override
  Widget build(BuildContext context) {
    final juegoProvider = Provider.of<JuegoProvider>(context);
    final juego = juegoProvider.getJuego(routName);
    final args = ModalRoute.of(context)?.settings.arguments as Map<String, String>;
    final evaAlumnoId = args != null ? args['evaAlumnoId'] : '';

    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: FadeInImage(
                      height: 128.0,
                      width: 128.0,
                      image: NetworkImage(juego.imgUrl),
                      placeholder: AssetImage('images/load-buff.gif'),
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Center(
                    child: Container(
                      height: 36.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.blueGrey[200]),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(Icons.timer_outlined),
                            Text(
                                '${juego.duracion.toString().padLeft(2, "0")} minutos'),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    '¿Cómo se juega?',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                        text: juego.descripcion,
                        style: TextStyle(color: Colors.black)),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    'Habilidades Evaluadas',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  ListView.builder(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: juego.habilidades.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Container(
                          height: 36.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.blueGrey[200],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(juego.habilidades[index]),
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Center(
                    child: ActionButton(
                      text: 'Iniciar',
                      onPressedAction: () {
                        Navigator.pushNamed(
                          context,
                          TocaCirculosSplash.routName,
                          arguments: <String, String>{
                            'evaAlumnoId': evaAlumnoId
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        'Toca Círculos',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
