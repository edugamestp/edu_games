import 'package:edu_games/screens/juego/CuentaCosasGame.dart';
import 'package:flutter/material.dart';

class CuentaCosasSplash extends StatefulWidget {
  static final String routName = 'splash-cuentacosas';

  @override
  _CuentaCosasSplashState createState() => _CuentaCosasSplashState();
}

class _CuentaCosasSplashState extends State<CuentaCosasSplash> {
  @override
  void initState() {
    super.initState();
    delayedTransition();
  }

  Future<void> delayedTransition() async {
    Future.delayed(Duration(seconds: 3), () => animatedTransition());
  }

  animatedTransition() {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secAnimation) {
          return CuentaCosasGame();
        },
        transitionDuration: Duration(seconds: 1),
        transitionsBuilder: (context, animation, secAnimation, child) {
          animation =
              CurvedAnimation(parent: animation, curve: Curves.easeInOut);
          return FadeTransition(
            opacity: Tween<double>(begin: 0.0, end: 1.0).animate(animation),
            child: child,
          );
        }));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Center(
            child: Text('Cuenta Cosas Splash'),
          ),
        ),
      ),
    );
  }
}
