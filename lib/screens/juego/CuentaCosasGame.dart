import 'dart:async';
import 'dart:math';

import 'package:edu_games/models/Pregunta.dart';
import 'package:edu_games/services/pregunta-service.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tts/flutter_tts.dart';

class CuentaCosasGame extends StatefulWidget {
  static final String routName = 'play-cuentacosas';

  @override
  _CuentaCosasGameState createState() => _CuentaCosasGameState();
}

class _CuentaCosasGameState extends State<CuentaCosasGame> {
  final PreguntaService _preguntaService = PreguntaService();
  FlutterTts _flutterTts = FlutterTts();
  List<int> _answers = [];
  List<Pregunta> _questions;
  bool _prepareMessage = true;
  bool _startQuestion = false;
  int _currentIdx;
  Pregunta _currentQuestion;
  List<int> _possibleAnswers = [];
  int _selectedAnswer = 99;
  bool _wasSelect = false;
  double _startPercent = 0.0;
  double _finalPercent = 0.0;
  int _hits = 0;
  int _maxHits = 0;
  bool _canSpeak = false;
  bool _isPlaying = false;


  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    configTts();
    playGame();
    super.initState();
  }

  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  Future<bool> popToPreviewPage() async {
    /* if (_canReturn) {
      Navigator.pop(context);
    } */
    Navigator.pop(context);
  }

  playGame() async {
    randomQuestions();

    await Future.delayed(Duration(seconds: 5), () {
      setState(() {
        _prepareMessage = false;
        _currentIdx = 0;
        _canSpeak = true;
        _isPlaying = true;
      });
    });

    buildQuestion();
  }

  configTts() async {
    await _flutterTts.setLanguage('es-ES');
    await _flutterTts.setPitch(1);
    await _flutterTts.awaitSpeakCompletion(true);
  }

  speakQuestion(String question) async {
    if (_canSpeak) await _flutterTts.speak(question);
  }

  randomQuestions() async {
    _answers = [2, 3, 4, 5, 6];
    _answers.shuffle();
    _questions = await _preguntaService.getPreguntas();    
    _questions.shuffle();

    for (int i = 0; i < 5; i++) {
      _questions[i].rptaCorrecta = _answers[i];
    }
  }

  buildQuestion() {
    setState(() {      
      _currentQuestion = _questions[_currentIdx];
      _possibleAnswers = [];
      _possibleAnswers.add(_currentQuestion.rptaCorrecta);
      while (_possibleAnswers.length < 3) {
        Random rdm = Random();
        final r = rdm.nextInt(10);
        if (_possibleAnswers.where((e) => e == r).toList().length == 0) {
          _possibleAnswers.add(r);
        }
      }
      _possibleAnswers.shuffle();
      _startQuestion = true;
    });
  }

  chooseNumber(int value) async {
    setState(() {
      _selectedAnswer = value;
      _wasSelect = true;
      _canSpeak = false;
      _isPlaying = false;
    });

    await Future.delayed(Duration(seconds: 3), () {
      setState(() {
        _currentIdx++;
      });
      if (_currentIdx < _questions.length) {
        setState(() {
          _canSpeak = true;
          _selectedAnswer = 99;
          _wasSelect = false;
          _isPlaying = true;
        });
        return buildQuestion();
      } else {
        return showEndDialog().then((value) => Navigator.pop(context));
      }
    });
  }

  Widget evaluatePlayer() {
    final correctAnswer = _currentQuestion.rptaCorrecta;
    _startPercent = _finalPercent;
    _maxHits++;
    if (_selectedAnswer == correctAnswer) {
      _hits++;
    }
    _finalPercent = _hits / _maxHits;
    return AnimatedProgressIndicator(
      initValue: _startPercent,
      finalValue: _finalPercent,
    );
  }

  Widget showQuestion(Size mediaSize) {
    speakQuestion(_currentQuestion.pregunta);
    return Container(
      width: mediaSize.width * 0.7,
      height: mediaSize.height * 0.8,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: _ImageQuestion(
              imgUrl: _currentQuestion
                  .imagenRespuesta[_currentQuestion.rptaCorrecta],
              mediaSize: mediaSize,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _CircleNumber(
                  answer: _possibleAnswers[0],
                  color: _selectedAnswer == _possibleAnswers[0]
                      ? Colors.blue[300]
                      : Colors.white,
                  onPressedAction: !_wasSelect
                      ? () => chooseNumber(_possibleAnswers[0])
                      : () {},
                ),
                _CircleNumber(
                  answer: _possibleAnswers[1],
                  color: _selectedAnswer == _possibleAnswers[1]
                      ? Colors.blue[300]
                      : Colors.white,
                  onPressedAction: !_wasSelect
                      ? () => chooseNumber(_possibleAnswers[1])
                      : () {},
                ),
                _CircleNumber(
                  answer: _possibleAnswers[2],
                  color: _selectedAnswer == _possibleAnswers[2]
                      ? Colors.blue[300]
                      : Colors.white,
                  onPressedAction: !_wasSelect
                      ? () => chooseNumber(_possibleAnswers[2])
                      : () {},
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future showEndDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: Container(
            height: 250,
            width: 400,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0), color: Colors.white),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '¡Juego Terminado!',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 24.0,
                      ),
                      ActionButton(
                        text: 'Salir',
                        fontSize: 16.0,
                        onPressedAction: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 45.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(45.0)),
                      child: Image.asset('images/congrats.png'),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: RotatedBox(
                    quarterTurns: 3,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 45.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(45.0)),
                        child: Image.asset('images/congrats.png'),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: popToPreviewPage,
      child: Scaffold(
        body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.green[300], Colors.yellow[300]],
                stops: [0, 1],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
              ),
            ),
            child: Stack(
              children: [
                if (_prepareMessage)
                  Align(
                    alignment: Alignment.center,
                    child: _PrepareMessage(mediaSize: mediaSize),
                  ),
                if (_startQuestion)
                  Align(
                    alignment: Alignment(-0.5, 0),
                    child: showQuestion(mediaSize),
                  ),
                Align(
                  alignment: Alignment(0.8, -0.85),
                  child: _ChronoTimer(
                    mediaSize: mediaSize,
                    isPlaying: _isPlaying,
                  ),
                ),
                Align(
                  alignment: Alignment(0.8, 0.3),
                  child: Material(
                    elevation: 4,
                    borderRadius: BorderRadius.circular(5.0),
                    child: Container(
                      width: mediaSize.width * 0.07,
                      height: mediaSize.height * 0.7,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: RotatedBox(
                          quarterTurns: -1,
                          child: _wasSelect
                              ? evaluatePlayer()
                              : LinearProgressIndicator(
                                  value: _finalPercent,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.blue[400]),
                                  backgroundColor: Colors.white,
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0.8, 0.1),
                  child: Container(
                    alignment: Alignment.center,
                    width: mediaSize.width * 0.07,
                    height: mediaSize.height * 0.1,
                    child: Text(
                      (_finalPercent * 100).toStringAsFixed(0) + '%',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ChronoTimer extends StatefulWidget {
  _ChronoTimer({
    Key key,
    @required this.mediaSize,
    @required this.isPlaying,
  }) : super(key: key);

  final Size mediaSize;
  final bool isPlaying;

  @override
  __ChronoTimerState createState() => __ChronoTimerState();
}

class __ChronoTimerState extends State<_ChronoTimer> {
  final Duration tick = Duration(seconds: 1);
  Timer timer;
  int secondsPassed = 0;

  handleTick() {
    if (widget.isPlaying) {
      setState(() {
        secondsPassed += 1;
      });
    }
  }

  startTimer() {
    if (timer == null) {
      timer = Timer.periodic(tick, (Timer t) {
        handleTick();
      });
    }
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  dispose() {
    timer.cancel();
    timer = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(5.0),
      child: Container(
        alignment: Alignment.center,
        width: widget.mediaSize.width * 0.07,
        height: widget.mediaSize.height * 0.07,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.green[100],
        ),
        child: Text(
          '${(secondsPassed ~/ 60).toString().padLeft(2, '0')}:${(secondsPassed % 60).toString().padLeft(2, '0')}',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

class _CircleNumber extends StatelessWidget {
  const _CircleNumber({
    this.answer,
    this.onPressedAction,
    this.color,
  });

  final int answer;
  final Color color;
  final GestureTapCallback onPressedAction;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressedAction,
      child: Text(
        answer.toString(),
        style: TextStyle(
          color: Colors.black,
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      style: ElevatedButton.styleFrom(
        elevation: 4,
        shape: CircleBorder(),
        padding: EdgeInsets.all(20),
        primary: color,
        onPrimary: Colors.blue,
      ),
    );
  }
}

class _ImageQuestion extends StatelessWidget {
  const _ImageQuestion({
    Key key,
    @required this.imgUrl,
    @required this.mediaSize,
  }) : super(key: key);

  final String imgUrl;
  final Size mediaSize;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(20.0),
      elevation: 4.0,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: FadeInImage(
          image: NetworkImage(imgUrl),
          height: mediaSize.height * 0.55,
          width: mediaSize.height * 0.55,
          placeholder: AssetImage('images/load-buff.gif'),
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}

class _PrepareMessage extends StatelessWidget {
  const _PrepareMessage({
    Key key,
    @required this.mediaSize,
  }) : super(key: key);

  final Size mediaSize;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(20.0),
      child: Container(
        alignment: Alignment.center,
        width: mediaSize.width * 0.4,
        height: mediaSize.height * 0.4,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0), color: Colors.white),
        child: Text(
          '!Escucha atentamente!',
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class AnimatedProgressIndicator extends StatefulWidget {
  final double initValue;
  final double finalValue;

  AnimatedProgressIndicator({this.initValue, this.finalValue});

  @override
  _AnimatedProgressIndicatorState createState() =>
      new _AnimatedProgressIndicatorState();
}

class _AnimatedProgressIndicatorState extends State<AnimatedProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1500), vsync: this);
    animation = Tween(begin: widget.initValue, end: widget.finalValue)
        .animate(controller)
          ..addListener(() {
            setState(() {});
          });
    controller.forward();
  }

  @override
  void dispose() {
    controller.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: animation.value,
      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue[400]),
      backgroundColor: Colors.white,
    );
  }
}