import 'package:edu_games/models/Habilidad.dart';
import 'package:edu_games/widgets/HabilityProgressBar.dart';
import 'package:flutter/material.dart';

class MyHabilitiesList extends StatelessWidget {
  final List<Habilidad> habilidades;

  MyHabilitiesList({this.habilidades});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: habilidades.length,
      itemBuilder: (BuildContext context, int index){
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: HabilityProgressBar(
            label: habilidades[index].habCognitiva,
            value: habilidades[index].puntaje,
            maxValue: 5,
          ),
        );
      },
    );
  }
}