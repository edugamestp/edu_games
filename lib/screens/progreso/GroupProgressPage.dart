import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/progreso/AlumnosGroupList.dart';

import 'package:edu_games/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GroupProgressPage extends StatelessWidget {
  static final String routName = 'group-progress';

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final grupo = grupoProvider.selectedGrupo;

    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(grupo.nombre),
        body: Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Alumnos',
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Expanded(
                    child: AlumnosGroupList(
                  alumnos: grupo.alumnos,
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  final String title;

  _TitleBar(this.title);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
