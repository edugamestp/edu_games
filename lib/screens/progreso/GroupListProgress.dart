import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/progreso/GroupProgressPage.dart';
import 'package:edu_games/widgets/TileGroupProgress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GroupListProgress extends StatelessWidget {
  final List<Grupo> grupos;

  GroupListProgress({this.grupos});

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);

    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: grupos.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: InkWell(                            
            onTap: () {
              grupoProvider.selectedGrupo = grupos[index];
              Navigator.pushNamed(context, GroupProgressPage.routName);
            },
            borderRadius: BorderRadius.circular(20.0),            
            child: TileGroupProgress(
              grupo: grupos[index].nombre,
              actualAlumnos: grupos[index].actualAlumnos,
              totalAlumnos: grupos[index].totalAlumnos,
            ),
          ),
        );
      },
    );
  }
}
