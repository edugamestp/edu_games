import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/widgets/TileEvaluationProgress.dart';
import 'package:flutter/material.dart';

class EvaluationListProgress extends StatelessWidget {  
  final List<Evaluacion> evaluaciones;

  EvaluationListProgress({this.evaluaciones});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: evaluaciones.length,
      itemBuilder: (BuildContext context, int index){
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: TileEvaluationProgress(
            evaluacion: evaluaciones[index].nombre,
            alumnos: evaluaciones[index].alumnos,
          ),
        );
      },
    );
  }
}