import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/models/Habilidad.dart';
import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/providers/evaluacion-alumno-provider.dart';
import 'package:edu_games/providers/evaluacion-provider.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/providers/habilidad-provider.dart';
import 'package:edu_games/providers/usuario-provider.dart';
import 'package:edu_games/screens/progreso/EvaluationListProgress.dart';
import 'package:edu_games/screens/progreso/GroupListProgress.dart';
import 'package:edu_games/screens/progreso/MyEvaluationsList.dart';
import 'package:edu_games/screens/progreso/MyHabilitiesList.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:edu_games/widgets/ProfileLabel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProgresoPage extends StatefulWidget {
  static final String routName = 'progreso';

  @override
  _ProgresoPageState createState() => _ProgresoPageState();
}

class _ProgresoPageState extends State<ProgresoPage> {
  final SharedPreferenceService _prefs = SharedPreferenceService();
  String _imgUrl = '';
  String _searchKey = '';

  @override
  void initState() {
    if (_prefs.userRole == 'docente') {
      _imgUrl = _prefs.userGenero == 'Masculino'
          ? 'images/teacher-male.png'
          : 'images/teacher-female.png';
    } else {
      _imgUrl = 'images/children.png';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final evaAlumnoProvider = Provider.of<EvaluacionAlumnoProvider>(context);
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final habilidadProvider = Provider.of<HabilidadProvider>(context);
    final usuarioProvider = Provider.of<UsuarioProvider>(context);
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    if (_prefs.userRole == 'alumno') {
      return SingleChildScrollView(
        child: Container(
          //height: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FutureBuilder(
                  future: usuarioProvider.getUsuario(),
                  builder:
                      (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
                    if (snapshot.hasData) {
                      final usuario = snapshot.data;
                      return ProfileLabel(
                        imgUrl: _imgUrl,
                        nombres: usuario.nombres + ' ' + usuario.apellidos,
                        edad: calculateAge(usuario.fechaNacimiento),
                      );
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                  child: Text(
                    'Mis Evaluaciones',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                FutureBuilder(
                  future: evaAlumnoProvider.getEvaluacionesByAlumno(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<EvaluacionAlumno>> snapshot) {
                    if (snapshot.hasData) {
                      final evaluaciones = snapshot.data;
                      return evaluaciones.isEmpty
                          ? Center(child: Text('Sin evaluaciones'))
                          : MyEvaluationsList(evaluaciones: evaluaciones);
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                  child: Text(
                    'Mi Desarrollo',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                FutureBuilder(
                  future: habilidadProvider.getHabilidades(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Habilidad>> snapshot) {
                    if (snapshot.hasData) {
                      final habilidades = snapshot.data;
                      return habilidades.isEmpty
                          ? Center(child: Text('Sin habilidades evaluadas'))
                          : MyHabilitiesList(habilidades: snapshot.data);
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FutureBuilder(
                  future: usuarioProvider.getUsuario(),
                  builder:
                      (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
                    if (snapshot.hasData) {
                      final usuario = snapshot.data;
                      return ProfileLabel(
                        imgUrl: _imgUrl,
                        nombres: usuario.nombres + ' ' + usuario.apellidos,
                        edad: calculateAge(usuario.fechaNacimiento),
                      );
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                  child: Text(
                    'Mis Grupos',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                FutureBuilder(
                  future: grupoProvider.getGruposByDocente(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Grupo>> snapshot) {
                    final grupos = grupoProvider.grupos;
                    if (grupos.length > 0) {
                      return GroupListProgress(
                        grupos: grupoProvider.grupos,
                      );
                    } else {
                      return SizedBox(
                        height: mediaSize.height * 0.5,
                        child: Center(
                          child: Text(
                              'Lo sentimos, aún no se te ha asignado a un grupo'),
                        ),
                      );
                    }
                  },
                ),
                FutureBuilder(
                  future: evaluacionProvider
                      .getQueryEvaluacionesByDocente(_searchKey),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Evaluacion>> snapshot) {
                    if (snapshot.hasData) {
                      final evaluaciones = snapshot.data;
                      return Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 16.0, bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Mis Evaluaciones',
                                  style: TextStyle(
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                OutlineActionButton(
                                  text: 'Filtrar',
                                  onPressedAction: () {
                                    return showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return _DialogFilterEvaluation();
                                      },
                                    ).then((value) {
                                      if (value != null) {
                                        final String key = value[0];
                                        final List<dynamic> data = value[1];
                                        if (data.length > 0) {
                                          setState(() {
                                            _searchKey = key;
                                          });
                                        } else {
                                          _searchKey = '';
                                          showSnack(Icons.cancel_outlined,
                                              'No se encontraron resultados');
                                        }
                                      }
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          EvaluationListProgress(evaluaciones: evaluaciones),
                        ],
                      );
                    } else {
                      return SizedBox(
                        height: 2.0,
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  int calculateAge(DateTime fechaNacimiento) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - fechaNacimiento.year;
    int currentMonth = currentDate.month;
    int birthMonth = fechaNacimiento.month;

    if (birthMonth > currentMonth) {
      age--;
    } else if (currentMonth == birthMonth) {
      int currentDay = currentDate.day;
      int birthDay = fechaNacimiento.day;
      if (birthDay > currentDay) {
        age--;
      }
    }

    return age;
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _DialogFilterEvaluation extends StatefulWidget {
  @override
  __DialogFilterEvaluationState createState() =>
      __DialogFilterEvaluationState();
}

class __DialogFilterEvaluationState extends State<_DialogFilterEvaluation> {
  String _nombreEvaluacion;

  @override
  Widget build(BuildContext context) {
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        height: 200.0,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Filtrar Evaluaciones',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  icon: Icon(
                    Icons.text_fields,
                    color: kSecondaryColor,
                    size: 18.0,
                  ),
                  labelText: 'Evaluación',
                  hintText: 'Ingrese nombre de la Evaluación',
                  hintStyle: TextStyle(color: Colors.grey[400]),
                ),
                onChanged: (value) => _nombreEvaluacion = value,
              ),
              ActionButton(
                text: 'Buscar',
                minWidth: 150,
                minHeight: 36,
                fontSize: 16,
                onPressedAction: () async {
                  final response = await evaluacionProvider
                      .getQueryEvaluacionesByDocente(_nombreEvaluacion);
                  return Navigator.of(context)
                      .pop([_nombreEvaluacion, response]);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
