import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/widgets/TileEvaluacionAlumno.dart';
import 'package:flutter/material.dart';

class MyEvaluationsList extends StatelessWidget {
  final List<EvaluacionAlumno> evaluaciones;

  MyEvaluationsList({this.evaluaciones});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: evaluaciones.length,
      itemBuilder: (BuildContext context, int index){
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: TileEvaluacionAlumno(
            nombreEvaluacion: evaluaciones[index].nombreEvaluacion,
            estado: evaluaciones[index].estado,
          ),
        );
      },
    );
  }
}
