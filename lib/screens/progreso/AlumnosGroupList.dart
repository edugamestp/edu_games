import 'package:edu_games/widgets/TileStudentProgress.dart';
import 'package:flutter/material.dart';

class AlumnosGroupList extends StatelessWidget {
  final List<String> alumnos;

  AlumnosGroupList({this.alumnos});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: alumnos.length,
      itemBuilder: (BuildContext context, int index){
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: TileStudentProgress(nombre: alumnos[index],),
        );
      },
    );
  }
}