import 'package:edu_games/screens/main/MainPage.dart';
import 'package:edu_games/screens/login/PreLoginPage.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key key}) : super(key: key);
  static final String routName = 'welcome';

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final prefs = SharedPreferenceService();

  @override
  void initState() {
    super.initState();     
    delayedTransition();
  }

  Future<void> delayedTransition() async {        
    Future.delayed(Duration(seconds: 2), () => animatedTransition());
  }

  animatedTransition() {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secAnimation) {
          if(prefs.userId == '') {
            return PreLoginPage();
          } else {
            return MainPage();
          }          
        },
        transitionDuration: Duration(seconds: 1),
        transitionsBuilder: (context, animation, secAnimation, child) {
          animation = CurvedAnimation(parent: animation, curve: Curves.easeInOut);
          return FadeTransition(
            opacity: Tween<double>(begin: 0.0, end: 1.0).animate(animation),
            child: child,
          );
        }));
  }

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: mediaSize.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [kSecondaryColor, Colors.white],
              stops: [0, 1],
              begin: Alignment(1, -0.98),
              end: Alignment(-1, 0.98)
            )
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: mediaSize.width * 0.5,
                height: mediaSize.width * 0.5,
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 10,
                    offset: Offset(0, 4),
                    )
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: ClipRRect(
                    child: Image.asset(
                      'images/logo-edu-games.jpg',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
