import 'package:edu_games/screens/entrenamiento/TrainingPage.dart';
import 'package:edu_games/screens/evaluacion/EvaluationPage.dart';
import 'package:edu_games/screens/grupo/GroupPage.dart';
import 'package:edu_games/screens/perfil/ProfilePage.dart';
import 'package:edu_games/screens/progreso/ProgresoPage.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/utils/scaffold-util.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  static final String routName = 'main';
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ScaffoldUtil(ScaffoldMessenger.of(context))),
        ChangeNotifierProvider(create: (_) => _NavigationModel(),),
      ],
      child: SafeArea(
        child: Scaffold(
          appBar: _TitleBar(),
          body: _Pages(),
          bottomNavigationBar: _BottomNavBar(),
        ),
      ),
    );
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    final navigationModel = Provider.of<_NavigationModel>(context);
    return AppBar(
      automaticallyImplyLeading: false,      
      title: Text(
        '${navigationModel.currentTitle}',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,      
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}

class _Pages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navigationModel = Provider.of<_NavigationModel>(context);

    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: navigationModel.pageController,
      children: [
        GroupPage(),
        EvaluationPage(),
        ProgresoPage(),
        TrainingPage(),
        ProfilePage(),
      ],
    );
  }
}

class _BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navigationModel = Provider.of<_NavigationModel>(context);

    return BottomNavigationBar(
      currentIndex: navigationModel.currentPage,
      onTap: (value) => navigationModel.currentPage = value,
      selectedItemColor: kPrimaryColor,
      unselectedItemColor: Colors.grey,
      showUnselectedLabels: true,
      items: [
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.users),
          label: 'Grupo',
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.chalkboardTeacher),
          label: 'Evaluación',
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.bookReader),
          label: 'Progreso',
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.cubes),
          label: 'Entrenamiento',
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.addressCard),
          label: 'Perfil',
        ),
      ],
    );
  }
}

class _NavigationModel with ChangeNotifier {
  List<String> _titles = [
    'Grupo',
    'Evaluación',
    'Progreso',
    'Entrenamiento',
    'Perfil'
  ];
  String _currentTitle = 'Progreso';
  int _currentPage = 2;
  PageController _pageController = PageController(initialPage: 2);

  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;
    _currentTitle = _titles[value];
    _pageController.animateToPage(
      value,
      duration: Duration(milliseconds: 250),
      curve: Curves.easeOut,
    );
    notifyListeners();
  }

  PageController get pageController => _pageController;

  String get currentTitle => _currentTitle;
}
