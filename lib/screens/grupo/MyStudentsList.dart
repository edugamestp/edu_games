import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:edu_games/widgets/TileStudent.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyStudentsList extends StatelessWidget {
  final List<String> alumnos;

  MyStudentsList({this.alumnos});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: alumnos.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: TileStudent(
            nombre: alumnos[index],
            onPressedAction: () {
              return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return _DialogRemoveStudent(alumno: alumnos[index]);
                },
              ).then((value) {
                if (value != null) {
                  value
                      ? showSnack(
                          Icons.thumb_up,
                          'Alumno eliminado con éxito!',
                          context,
                        )
                      : showSnack(
                          Icons.cancel_outlined,
                          'Error al eliminar el alumno',
                          context,
                        );
                }
              });
            },
          ),
        );
      },
    );
  }

  showSnack(IconData icon, String message, BuildContext context) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}


class _DialogRemoveStudent extends StatelessWidget {
  final String alumno;

  _DialogRemoveStudent({this.alumno});

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final mediaSize = MediaQuery.of(context).size;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        height: mediaSize.height * 0.30,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Atención!',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '¿Está seguro de eliminar al alumno del grupo?',
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ActionButton(
                    text: 'Sí',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () async {
                      final wasUpdated =
                          await grupoProvider.updateAlumnos(alumno);
                      Navigator.of(context).pop(wasUpdated);
                    },
                  ),
                  ActionButton(
                    text: 'No',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}