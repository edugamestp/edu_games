import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/grupo/CreateGroupPage.dart';
import 'package:edu_games/screens/grupo/MyGroupList.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GroupPage extends StatefulWidget {
  static final String routName = 'group';

  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  final SharedPreferenceService _prefs = SharedPreferenceService();

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);

    return _prefs.userRole == 'alumno'
        ? Center(
            child: Text('En desarrollo'),
          )
        : Container(
            height: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Mis Grupos',
                        style: TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      OutlineActionButton(
                        text: 'Agregar',
                        onPressedAction: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CreateGroupPage(
                                  grupo: null, rolePage: 'create'),
                            ),
                          ).then((value) => setState(() {
                                grupoProvider.getGruposByDocente();
                              }));
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  Expanded(
                    child: grupoProvider.isLoading
                        ? Center(child: CircularProgressIndicator())
                        : MyGroupList(grupos: grupoProvider.grupos),
                  ),
                ],
              ),
            ),
          );
  }
}
