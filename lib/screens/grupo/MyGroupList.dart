import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/grupo/CreateGroupPage.dart';
import 'package:edu_games/screens/grupo/EditStudentsPage.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/TileGrupo.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MyGroupList extends StatelessWidget {
  final List<Grupo> grupos;  

  MyGroupList({this.grupos});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: grupos.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: TileGrupo(
            grupo: grupos[index].nombre,
            aula: grupos[index].aula,
            actualAlumnos: grupos[index].actualAlumnos,
            totalAlumnos: grupos[index].totalAlumnos,            
            onPressedAction: () {
              showModalBottomSheet(
                context: context,
                builder: (context) {
                  return actions(
                    grupos[index],
                    context,
                  );
                },
              );
            },
          ),
        );
      },
    );
  }

  Widget actions(Grupo grupo, BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    return Container(
      height: 230,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0),
            child: Text('${grupo.nombre}(${grupo.aula})'),
          ),
          Divider(),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(FontAwesomeIcons.clipboardList,
                        color: kPrimaryColor),
                  ),
                  Text('Editar lista de alumnos'),
                ],
              ),
            ),
            onTap: () {
              grupoProvider.selectedGrupo = grupo;
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditStudentsPage(),
                ),
              ).then((value) => grupoProvider.getGruposByDocente());
            },
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(FontAwesomeIcons.solidEdit,
                        color: kPrimaryColor),
                  ),
                  Text('Editar grupo'),
                ],
              ),
            ),
            onTap: () {
              grupoProvider.selectedGrupo = grupo;
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      CreateGroupPage(grupo: grupo, rolePage: 'edit'),
                ),
              ).then((value) => grupoProvider.getGruposByDocente());
            },
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(FontAwesomeIcons.trash, color: kPrimaryColor),
                  ),
                  Text('Eliminar grupo'),
                ],
              ),
            ),
            onTap: () {
              grupoProvider.selectedGrupo = grupo;
              Navigator.pop(context);
              return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return _DialogDeleteGroup();
                },
              ).then((value) {
                grupoProvider.getGruposByDocente();
              });
            },
          ),
        ],
      ),
    );
  }
}

class _DialogDeleteGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        //mejorar el height
        height: mediaSize.height * 0.30,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Atención!',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '¿Está seguro de eliminar el grupo?',
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ActionButton(
                    text: 'Sí',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () async {
                      final wasDeleted = await grupoProvider.deleteGrupo();
                      Navigator.of(context).pop(wasDeleted);
                    },
                  ),
                  ActionButton(
                    text: 'No',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
