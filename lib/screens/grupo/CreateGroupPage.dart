import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CreateGroupPage extends StatefulWidget {
  static final String routName = 'create-group';
  final Grupo grupo;
  final String rolePage;

  CreateGroupPage({this.grupo, this.rolePage});

  @override
  _CreateGroupPageState createState() => _CreateGroupPageState();
}

class _CreateGroupPageState extends State<CreateGroupPage> {
  final _formKey = GlobalKey<FormState>();  
  Grupo _grupo;

  @override
    void initState() {
      _grupo = widget.grupo ?? Grupo();      
      super.initState();
    }

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);

    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(rolePage: widget.rolePage),
        body: SingleChildScrollView(
          child: Container(            
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
              child: Column(   
                mainAxisAlignment: MainAxisAlignment.spaceBetween,                         
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        createInputGrupo(),
                        SizedBox(height: 24),
                        createInputAula(),
                      ],
                    ),
                  ),
                  Center(                  
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 48.0),
                      child: ActionButton(
                        text: widget.rolePage == 'create' ? 'Crear' : 'Editar',
                        minWidth: 180,
                        minHeight: 36,
                        fontSize: 16,
                        onPressedAction: () {  
                          saveGrupo(grupoProvider);
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget createInputGrupo() {
    return TextFormField(
      autofocus: false,
      initialValue: _grupo.nombre ?? '',      
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon:
            FaIcon(FontAwesomeIcons.users, color: kSecondaryColor, size: 20.0),
        labelText: 'Grupo',
        hintText: 'Ingrese nombre del grupo',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _grupo.nombre = value,
    );
  }

  Widget createInputAula() {
    return TextFormField(
      autofocus: false,  
      initialValue: _grupo.aula ?? '',    
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon:
            FaIcon(FontAwesomeIcons.school, color: kSecondaryColor, size: 20.0),
        labelText: 'Aula',
        hintText: 'Ingrese nombre del aula',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value == _grupo.nombre) {
          return 'Nombre de grupo y aula no deben ser iguales';
        }
        return null;
      },
      onChanged: (value) => _grupo.aula = value,
    );
  }

  saveGrupo(GrupoProvider grupoProvider) async {
    if (_formKey.currentState.validate()) {
      bool response;
      if(widget.rolePage == 'create') response = await grupoProvider.createGrupo(_grupo);
      else response = await grupoProvider.updateGrupo(_grupo);      
      if (response) {
        final msg = widget.rolePage == 'create' ? 'Registro exitoso!' : 'Actualización exitosa!';
        showSnack(Icons.thumb_up, msg);
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } else {
        final msg = widget.rolePage == 'create' ? 'Error al registrar el grupo' : 'Error al actualizar el grupo';
        showSnack(Icons.cancel_outlined, msg);
      }      
    } else {
      showSnack(Icons.warning_amber_rounded, 'Ingrese los datos correctamente');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  final String rolePage;

  _TitleBar({this.rolePage});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        rolePage == 'create' ? 'Crear Grupo' : 'Editar Grupo',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
