import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/grupo/MyStudentsList.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditStudentsPage extends StatefulWidget {
  static final String routName = 'edit-alumnos';

  @override
  _EditStudentsPageState createState() => _EditStudentsPageState();
}

class _EditStudentsPageState extends State<EditStudentsPage> {
  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final grupo = grupoProvider.selectedGrupo;
    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(grupoNombre: grupo.nombre),
        body: Container(
          height: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Mis Alumnos',
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    OutlineActionButton(
                      text: 'Agregar',
                      onPressedAction: () {
                        return showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return _DialogAddStudent(grupo: grupo);
                          },
                        ).then((value) {
                          if (value == 'agregado') {
                            showSnack(
                                Icons.thumb_up, 'Alumno agregado con éxito!');
                            setState(() {
                              grupoProvider.getGruposByDocente();
                            });
                          } else if (value == 'no-existe') {
                            showSnack(
                                Icons.cancel_outlined, 'El Alumno no existe');
                          } else if (value == 'en-grupo') {
                            showSnack(Icons.warning_amber_rounded,
                                'El Alumno ya esta agregado a otro grupo');
                          } else if (value == 'error') {
                            showSnack(Icons.cancel_outlined,
                                'Error al agregar el Alumno');
                          } else {
                            return;
                          }
                        });
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 12.0,
                ),
                Expanded(
                  child: grupoProvider.isLoading
                      ? Center(child: CircularProgressIndicator())
                      : MyStudentsList(alumnos: grupo.alumnos),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  final String grupoNombre;

  _TitleBar({this.grupoNombre});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        grupoNombre,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}

class _DialogAddStudent extends StatefulWidget {
  final Grupo grupo;

  _DialogAddStudent({this.grupo});

  @override
  __DialogAddStudentState createState() => __DialogAddStudentState();
}

class __DialogAddStudentState extends State<_DialogAddStudent> {
  final _formKey = GlobalKey<FormState>();
  String _alumnoDni = '';

  @override
  Widget build(BuildContext context) {
    final _grupoProvider = Provider.of<GrupoProvider>(context);
    final mediaSize = MediaQuery.of(context).size;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        height: mediaSize.height * 0.35,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Agregar Alumno',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Form(
                key: _formKey,
                child: createInputDni(),
              ),
              ActionButton(
                text: 'Agregar',
                minWidth: 150,
                minHeight: 36,
                fontSize: 16,
                onPressedAction: () async {
                  if (_formKey.currentState.validate()) {
                    final response = await _grupoProvider.addAlumnoToGrupo(
                        widget.grupo, _alumnoDni);
                    return Navigator.of(context).pop(response);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget createInputDni() {
    return TextFormField(
      autofocus: false,
      maxLength: 8,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(
          Icons.call_to_action,
          color: kSecondaryColor,
          size: 18.0,
        ),
        labelText: 'Alumno DNI',
        hintText: 'Ingrese DNI del alumno',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value.length != 8) {
          return 'Número de DNI no válido';
        }
        return null;
      },
      onChanged: (value) => _alumnoDni = value,
    );
  }
}
