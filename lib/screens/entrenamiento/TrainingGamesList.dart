import 'package:edu_games/models/Juegos.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/TileJuego.dart';
import 'package:flutter/material.dart';

class TrainingGamesList extends StatelessWidget {
  final List<Juego> juegos;

  TrainingGamesList({this.juegos});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: juegos.length,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        final numColors = [1, 2, 3];
        final arrColors = [...numColors, ...numColors, ...numColors];
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(context, juegos[index].id);
            },
            borderRadius: BorderRadius.circular(20.0),
            child: TileJuego(
              nombreJuego: juegos[index].nombre,
              imgUrl: juegos[index].imgUrl,
              duracion: juegos[index].duracion,
              numHabilidades: juegos[index].habilidades.length,
              color: kColorsTile[arrColors[index]],
            ),
          ),
        );
      },
    );
  }
}
