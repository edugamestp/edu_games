import 'package:edu_games/models/Juegos.dart';
import 'package:edu_games/providers/juego-provider.dart';
import 'package:edu_games/screens/entrenamiento/TrainingGamesList.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TrainingPage extends StatefulWidget {
  static final String routName = 'training';

  @override
  _TrainingPageState createState() => _TrainingPageState();
}

class _TrainingPageState extends State<TrainingPage> {
  List<String> _filters = [];
  @override
  Widget build(BuildContext context) {
    final juegoProvider = Provider.of<JuegoProvider>(context);

    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Juegos',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  OutlineActionButton(
                    text: 'Filtrar',
                    onPressedAction: () {
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return _DialogFilterGames();
                        },
                      ).then((value) {
                        setState(() {
                          _filters = value;
                        });
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              FutureBuilder(
                future: juegoProvider.getQueryJuegos(_filters),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Juego>> snapshot) {
                  if (snapshot.hasData) {
                    final juegos = snapshot.data;
                    return TrainingGamesList(
                      juegos: juegos,
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _DialogFilterGames extends StatefulWidget {
  @override
  __DialogFilterGamesState createState() => __DialogFilterGamesState();
}

class __DialogFilterGamesState extends State<_DialogFilterGames> {
  List<Item> _habilidades = [
    Item(habilidad: 'Atención focalizada', isSelected: false),
    Item(habilidad: 'Campo visual', isSelected: false),
    Item(habilidad: 'Coordinación ojo-mano', isSelected: false),
    Item(habilidad: 'Denominación', isSelected: false),
    Item(habilidad: 'Exploración visual', isSelected: false),
    Item(habilidad: 'Memoria', isSelected: false),
    Item(habilidad: 'Percepción auditiva', isSelected: false),
    Item(habilidad: 'Percepción visual', isSelected: false),
    Item(habilidad: 'Tiempo de respuesta', isSelected: false),
  ];

  List<String> _habilidadesSeleccionadas = [];

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Habilidades cognitivas',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 12.0,
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: _habilidades.length,
                itemBuilder: (BuildContext context, int index) {
                  //bool selected = false;
                  return TileHabilidadItem(
                    habilidad: _habilidades[index].habilidad,
                    isSelected: _habilidades[index].isSelected,
                    onPressedAction: () {
                      setState(() {
                        _habilidades[index].isSelected =
                            !_habilidades[index].isSelected;
                        if (_habilidades[index].isSelected) {
                          _habilidadesSeleccionadas
                              .add(_habilidades[index].habilidad);
                        } else {
                          _habilidadesSeleccionadas
                              .remove(_habilidades[index].habilidad);
                        }
                      });
                    },
                  );
                },
              ),
              SizedBox(
                height: 12.0,
              ),
              ActionButton(
                text: 'Buscar',
                minWidth: 150,
                minHeight: 36,
                fontSize: 16,
                onPressedAction: () {
                  return Navigator.of(context).pop(_habilidadesSeleccionadas);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TileHabilidadItem extends StatelessWidget {
  final String habilidad;
  final bool isSelected;
  final GestureTapCallback onPressedAction;

  TileHabilidadItem({this.habilidad, this.isSelected, this.onPressedAction});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: InkWell(
        onTap: onPressedAction,
        child: Container(
          height: 36.0,
          child: Row(
            children: [
              isSelected
                  ? Icon(Icons.check_box, color: kPrimaryColor)
                  : Icon(Icons.check_box_outline_blank),
              SizedBox(
                width: 24.0,
              ),
              Text(habilidad),
            ],
          ),
        ),
      ),
    );
  }
}

class Item {
  String habilidad;
  bool isSelected;

  Item({this.habilidad, this.isSelected});
}
