import 'package:edu_games/models/Institucion.dart';
import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/Services/auth-service.dart';
import 'package:edu_games/screens/main/MainPage.dart';
import 'package:edu_games/services/institucion-service.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  static final String routName = 'register';
  final String role;
  const RegisterPage({this.role});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final SharedPreferenceService _prefs = SharedPreferenceService();
  final _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  final _formKey = GlobalKey<FormState>();
  final AuthService _authService = AuthService();
  final InstitucionService _institucionService = InstitucionService();
  List<String> _instituciones = [];
  String _institucionSelected;
  String _generoSelected;
  Usuario _usuario = Usuario();
  TextEditingController _fechaController = TextEditingController();

  @override
  void initState() {
    super.initState();
    setInstituciones();
    _generoSelected = 'Masculino';
    _usuario.genero = _generoSelected;
    _usuario.role = widget.role;
  }

  Future<void> setInstituciones() async {
    List<Institucion> institucionesRes =
        await _institucionService.getInstituciones();
    _instituciones = institucionesRes.map((e) => e.nombre).toList();
    if (widget.role == 'docente') {
      setState(() {
        _institucionSelected = _instituciones.first;
        _usuario.institucion = _institucionSelected;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldMessengerKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: TextButton(
            child: Icon(
              Icons.arrow_back_ios,
              color: kPrimaryColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'Registrarse',
            style: TextStyle(
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
                color: kPrimaryColor),
          ),
          backgroundColor: Colors.white,
          //elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Por favor introduzca sus datos personales',
                    style: TextStyle(fontSize: 16),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 24),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          createInputNombre(),
                          SizedBox(height: 24),
                          createInputApellidos(),
                          SizedBox(height: 24),
                          createInputDni(),
                          SizedBox(height: 24),
                          createInputGenero(),
                          SizedBox(height: 24),
                          createInputFechaNac(),
                          SizedBox(height: 24),
                          if (widget.role == 'docente') createInputTelefono(),
                          if (widget.role == 'docente') SizedBox(height: 24),
                          if (widget.role == 'docente')
                            createInputInstitucion(),
                          if (widget.role == 'docente') SizedBox(height: 24),
                          if (widget.role == 'docente') createInputCorreo(),
                          if (widget.role == 'docente') SizedBox(height: 24),
                          createInputContrasenya(),
                          SizedBox(height: 24),
                          createInputRepeatContrasenya(),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: ActionButton(
                      text: 'Registrarse',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        saveUsuario();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget createInput(
      {String label,
      String hint,
      String value,
      IconData icon,
      int maxLength,
      TextInputType type,
      bool isPassword}) {
    return TextFormField(
      autofocus: false,
      maxLength: maxLength,
      keyboardType: type,
      obscureText: isPassword,
      buildCounter: (BuildContext context,
              {int currentLength, int maxLength, bool isFocused}) =>
          null,
      initialValue: value,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(8.0),
          icon: Icon(icon),
          labelText: label,
          hintText: hint,
          hintStyle: TextStyle(color: Colors.grey[400])),
      //onSaved: (value) => nombres = value,
      //onChanged: (value) => nombres = value
    );
  }

  Widget createInputNombre() {
    return TextFormField(
      autofocus: false,
      maxLength: 30,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.person, color: kSecondaryColor),
        labelText: 'Nombres',
        hintText: 'Ingrese sus nombres',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _usuario.nombres = value,
    );
  }

  Widget createInputApellidos() {
    return TextFormField(
      autofocus: false,
      maxLength: 30,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.person, color: kSecondaryColor),
        labelText: 'Apellidos',
        hintText: 'Ingrese sus apellidos',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _usuario.apellidos = value,
    );
  }

  Widget createInputDni() {
    return TextFormField(
      autofocus: false,
      maxLength: 8,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.call_to_action, color: kSecondaryColor),
        labelText: 'DNI',
        hintText: 'Ingrese su DNI',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value.length != 8) {
          return 'El campo DNI debe de tener 8 dígitos';
        }
        return validateDni(value);
      },
      onChanged: (value) => _usuario.dni = value,
    );
  }

  String validateDni(String value) {
    Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null) {
      return 'Campo Número de DNI no válido';
    }
    return null;
  }

  Widget createInputGenero() {
    return FormField(
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            icon: Icon(Icons.people_sharp, color: kSecondaryColor),
            labelText: 'Género',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
          isEmpty: _generoSelected == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: _generoSelected,
              onChanged: (value) {
                FocusScope.of(context).requestFocus(FocusNode());
                _generoSelected = value;
                _usuario.genero = value;
                state.didChange(value);
              },
              items: generoDropDownItems(),
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<String>> generoDropDownItems() {
    List<DropdownMenuItem<String>> items = [];
    items.add(DropdownMenuItem(child: Text('Masculino'), value: 'Masculino'));
    items.add(DropdownMenuItem(child: Text('Femenino'), value: 'Femenino'));
    return items;
  }

  Widget createInputFechaNac() {
    return TextFormField(
      controller: _fechaController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        hintText: 'Fecha de Nacimiento',
        labelText: 'Fecha de Nacimiento',
        icon: Icon(Icons.today, color: kSecondaryColor),
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (_usuario.fechaNacimiento.year >=
            DateTime.now().year) {
          return 'La Fecha de nacimiento debe estar en pasado';
        }
        return null;
      },
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        selectDate(context);
      },
    );
  }

  selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime(2050),
      locale: Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        _usuario.fechaNacimiento = picked;
        String day = picked.day.toString().padLeft(2, '0');
        String month = picked.month.toString().padLeft(2, '0');
        String year = picked.year.toString();
        _fechaController.text = '$day-$month-$year';
      });
    }
  }

  Widget createInputTelefono() {
    return TextFormField(
      autofocus: false,
      maxLength: 9,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.phone, color: kSecondaryColor),
        labelText: 'Teléfono',
        hintText: 'Ingrese su número telefónico',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value.length < 7) {
          return 'Campo Número telefónico no válido';
        }
        return validateTelefono(value);
      },
      onChanged: (value) => _usuario.telefono = value,
    );
  }

  String validateTelefono(String value) {
    Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null) {
      return 'Campo Número telefónico no válido';
    }
    return null;
  }

  Widget createInputInstitucion() {
    return FormField(
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            icon: Icon(Icons.business_outlined, color: kSecondaryColor),
            labelText: 'Insitución',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
          isEmpty: _institucionSelected == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: _institucionSelected,
              onChanged: (value) {
                FocusScope.of(context).requestFocus(FocusNode());
                _institucionSelected = value;
                _usuario.institucion = value;
                state.didChange(value);
              },
              items: institucionDropDownItems(),
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<String>> institucionDropDownItems() {
    List<DropdownMenuItem<String>> items = [];
    _instituciones.forEach((opt) {
      items.add(
        DropdownMenuItem(child: Text(opt), value: opt),
      );
    });
    return items;
  }

  Widget createInputCorreo() {
    return TextFormField(
      autofocus: false,
      maxLength: 50,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.email, color: kSecondaryColor),
        labelText: 'Correo',
        hintText: 'Ingrese su correo electrónico',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return validateEmail(value);
      },
      onChanged: (value) => _usuario.correo = value,
    );
  }

  Widget createInputContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.lock, color: kSecondaryColor),
        labelText: 'Contraseña',
        hintText: 'Ingrese su contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if(value.length > 20) {
          return 'La contraseña debe tener un máximo de 20 caracteres';
        }
        return null;
      },
      onChanged: (value) => _usuario.contrasenya = value,
    );
  }

  Widget createInputRepeatContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(
          Icons.lock,
          color: kSecondaryColor,
        ),
        labelText: 'Repetir Contraseña',
        hintText: 'Vuelva a ingresar su contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value != _usuario.contrasenya) {
          return 'Las contraseñas no coinciden';
        }
        return null;
      },
      //onChanged: (value) => _usuario.contrasenya = value,
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null) {
      return 'Campo Correo electrónico no válido';
    }
    return null;
  }

  saveUsuario() async {
    if (_formKey.currentState.validate()) {
      final response = await _authService.postUsuario(_usuario);
      if (response.length > 0) {
        _usuario.id = response;
        _prefs.saveData(_usuario);
        showSnack(Icons.thumb_up, 'Registro exitoso!');         
        Future.delayed(
            Duration(seconds: 2),
            () => Navigator.pushNamedAndRemoveUntil(
                context, MainPage.routName, (route) => false));
      } else {
        showSnack(Icons.cancel_outlined, 'Error al registrar sus datos');
      }
    } else {
      showSnack(Icons.warning_amber_rounded, 'Ingrese sus datos correctamente');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}
