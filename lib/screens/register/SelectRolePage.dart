import 'package:edu_games/screens/register/RegisterPage.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/RoleCard.dart';
import 'package:flutter/material.dart';

class SelectRolePage extends StatefulWidget {
  static final String routName = 'select-role';

  @override
  _SelectRolePageState createState() => _SelectRolePageState();
}

class _SelectRolePageState extends State<SelectRolePage> {
  String selectRole = '';

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Align(
              alignment: Alignment(0, -0.7),
              child: Text(
                'Selecciona tu rol',
                style: TextStyle(fontSize: 34, fontWeight: FontWeight.bold),
              ),
            ),
            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: mediaSize.height * 0.4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectRole = 'docente';
                        });
                      },
                      child: RoleCard(
                        imgPath: 'images/teacher-female.png',
                        label: 'Docente',
                        color: selectRole == 'docente'
                            ? kSecondaryColor
                            : Colors.white,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectRole = 'alumno';
                        });
                      },
                      child: RoleCard(
                        imgPath: 'images/children.png',
                        label: 'Alumno',
                        color: selectRole == 'alumno'
                            ? kSecondaryColor
                            : Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Align(
                alignment: Alignment(0, 0.7),
                child: ActionButton(
                  text: 'Continuar',
                  minWidth: 180,
                  minHeight: 36,
                  fontSize: 16,
                  onPressedAction: () {
                    if (selectRole.length > 0) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RegisterPage(role: selectRole),
                        ),
                      );
                    }
                  },
                )),
          ],
        ),
      ),
    );
  }
}
