import 'package:flutter/material.dart';

class DetailInformation extends StatelessWidget {
  final String label;
  final String text;

  DetailInformation({this.label, this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Row(
        children: [
          Container(
            width: 130.0,
            child: Text(
              label,
              style: TextStyle(fontSize: 16.0, color: Colors.grey),
            ),
          ),
          Text(
            text,
            style: TextStyle(fontSize: 16.0),
          ),
        ],
      ),
    );
  }
}
