import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/services/auth-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/material.dart';

class ChangePasswordPage extends StatefulWidget {
  static final String routName = 'change-password';
  final Usuario usuario;

  ChangePasswordPage({this.usuario});

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final _formKey = GlobalKey<FormState>();
  final AuthService _authService = AuthService();
  Usuario _usuario = Usuario();
  String _oldContrasenya = '';
  String _newContrasenya = '';

  @override
  void initState() {
    _usuario = widget.usuario;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 24),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          createInputContrasenya(),
                          SizedBox(height: 24),
                          createInputNewContrasenya(),
                          SizedBox(height: 24),
                          createInputRepeatNewContrasenya(),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: ActionButton(
                      text: 'Actualizar',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {                        
                        updateContrasenya();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget createInputContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.lock, color: kSecondaryColor),
        labelText: 'Contraseña',
        hintText: 'Ingrese su contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _oldContrasenya = value,
    );
  }

  Widget createInputNewContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.lock, color: kSecondaryColor),
        labelText: 'Nueva Contraseña',
        hintText: 'Ingrese su nueva contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _newContrasenya = value,
    );
  }

  Widget createInputRepeatNewContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(
          Icons.lock,
          color: kSecondaryColor,
        ),
        labelText: 'Repetir Nueva Contraseña',
        hintText: 'Vuelva a ingresar su nueva contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value != _newContrasenya) {
          return 'Las contraseñas no coinciden';
        }
        return null;
      },
    );
  }

  updateContrasenya() async {
    if (_formKey.currentState.validate()) {
      final response = await _authService.updateContrasenya(_usuario, _oldContrasenya, _newContrasenya);
      if (response) {
        showSnack(Icons.thumb_up, 'Actualización exitosa!');
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } else {
        showSnack(Icons.cancel_outlined, 'Error al actualizar la contraseña');
      }
    } else {
      showSnack(
          Icons.warning_amber_rounded, 'Ingrese sus contraseñas correctamente');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () {          
          Navigator.pop(context);
        },
      ),
      title: Text(
        'Cambiar Contraseña',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
