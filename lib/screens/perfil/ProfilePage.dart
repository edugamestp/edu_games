import 'package:edu_games/providers/usuario-provider.dart';
import 'package:edu_games/screens/perfil/ChangePasswordPage.dart';
import 'package:edu_games/screens/perfil/DetailInformation.dart';
import 'package:edu_games/screens/perfil/EditProfilePage.dart';
import 'package:edu_games/screens/welcome/WelcomePage.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/date-util.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  static final String routName = 'profile';

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _prefs = SharedPreferenceService();
  String _textoUsuario = '';
  String _imgUrl = '';

  @override
  void initState() {
    setData();
    super.initState();
  }

  setData() {
    if (_prefs.userRole == 'docente') {
      _textoUsuario = 'Docente';
      _imgUrl = _prefs.userGenero == 'Masculino'
          ? 'images/teacher-male.png'
          : 'images/teacher-female.png';
    } else {
      _textoUsuario = _prefs.userGenero == 'Masculino' ? 'Alumno' : 'Alumna';
      _imgUrl = 'images/children.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    final usuarioProvider = Provider.of<UsuarioProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Column(            
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: CircleAvatar(
                  radius: mediaSize.width * 0.15,
                  backgroundImage: AssetImage(_imgUrl),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Column(                  
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _textoUsuario,
                          style: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        OutlineActionButton(
                          text: 'Editar',
                          onPressedAction: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditProfilePage(
                                    usuario: usuarioProvider.usuario),
                              ),
                            ).then((_) {
                              setData();
                            });
                          },
                        ),
                      ],
                    ),
                    Container(
                      child: Column(
                        children: [
                          DetailInformation(
                              label: 'Nombres',
                              text: usuarioProvider.usuario.nombres),
                          DetailInformation(
                              label: 'DNI', text: usuarioProvider.usuario.dni),
                          DetailInformation(
                              label: 'Género',
                              text: usuarioProvider.usuario.genero),
                          DetailInformation(
                              label: 'Fec. de Nac.',
                              text: formatDateUtil(
                                  usuarioProvider.usuario.fechaNacimiento)),
                          if (_prefs.userRole == 'docente')
                            DetailInformation(
                                label: 'Teléfono',
                                text: usuarioProvider.usuario.telefono),
                          if (_prefs.userRole == 'docente')
                            DetailInformation(
                                label: 'Inst. Educativa',
                                text: usuarioProvider.usuario.institucion),
                          if (_prefs.userRole == 'docente')
                            DetailInformation(
                                label: 'Correo',
                                text: usuarioProvider.usuario.correo),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 48.0,
                    ),
                    ActionButton(
                      text: 'Cambiar Contraseña',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChangePasswordPage(
                                usuario: usuarioProvider.usuario),
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    ActionButton(
                      text: 'Cerrar Sesión',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        logoutUsuario();
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  logoutUsuario() async {
    await _prefs.clearData();
    Navigator.pushNamedAndRemoveUntil(
        context, WelcomePage.routName, (route) => false);
  }
}
