import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/services/usuario-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/material.dart';

class EditProfilePage extends StatefulWidget {
  static final String routName = 'edit-profile';
  final Usuario usuario;

  EditProfilePage({this.usuario});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final SharedPreferenceService _prefs = SharedPreferenceService();
  final _formKey = GlobalKey<FormState>();
  final UsuarioService _usuarioService = UsuarioService();
  Usuario _usuario = Usuario();
  String _generoSelected;
  TextEditingController _fechaController = TextEditingController();

  @override
  void initState() {
    _usuario = widget.usuario;
    _generoSelected = _usuario.genero;
    String day = _usuario.fechaNacimiento.day.toString().padLeft(2, '0');
    String month = _usuario.fechaNacimiento.month.toString().padLeft(2, '0');
    String year = _usuario.fechaNacimiento.year.toString();
    _fechaController.text = '$day-$month-$year';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Por favor actualice los datos necesarios',
                    style: TextStyle(fontSize: 16),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 24),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          createInputNombre(),
                          SizedBox(height: 24),
                          createInputApellidos(),
                          SizedBox(height: 24),
                          createInputGenero(),
                          SizedBox(height: 24),
                          createInputFechaNac(),
                          SizedBox(height: 24),
                          if (_usuario.role == 'docente') createInputTelefono(),
                          if (_usuario.role == 'docente') SizedBox(height: 24),
                          if (_usuario.role == 'docente') createInputCorreo(),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: ActionButton(
                      text: 'Actualizar',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        updateUsuario();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget createInputNombre() {
    return TextFormField(
      initialValue: _usuario.nombres,
      autofocus: false,
      maxLength: 30,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.person, color: kSecondaryColor),
        labelText: 'Nombres',
        hintText: 'Ingrese sus nombres',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _usuario.nombres = value,
    );
  }

  Widget createInputApellidos() {
    return TextFormField(
      initialValue: _usuario.apellidos,
      autofocus: false,
      maxLength: 30,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.person, color: kSecondaryColor),
        labelText: 'Apellidos',
        hintText: 'Ingrese sus apellidos',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _usuario.apellidos = value,
    );
  }

  Widget createInputGenero() {
    return FormField(
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            icon: Icon(Icons.people_sharp, color: kSecondaryColor),
            labelText: 'Género',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
          isEmpty: _generoSelected == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: _generoSelected,
              onChanged: (value) {
                FocusScope.of(context).requestFocus(FocusNode());
                _generoSelected = value;
                _usuario.genero = value;
                state.didChange(value);
              },
              items: generoDropDownItems(),
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<String>> generoDropDownItems() {
    List<DropdownMenuItem<String>> items = [];
    items.add(DropdownMenuItem(child: Text('Masculino'), value: 'Masculino'));
    items.add(DropdownMenuItem(child: Text('Femenino'), value: 'Femenino'));
    return items;
  }

  Widget createInputFechaNac() {
    return TextFormField(
      controller: _fechaController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        hintText: 'Fecha de Nacimiento',
        labelText: 'Fecha de Nacimiento',
        icon: Icon(Icons.today, color: kSecondaryColor),
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (_usuario.fechaNacimiento.year >=
            DateTime.now().year) {
          return 'La Fecha de nacimiento debe estar en pasado';
        }
        return null;
      },
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        selectDate(context);
      },
    );
  }

  selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: _usuario.fechaNacimiento,
      firstDate: DateTime(1950),
      lastDate: DateTime(2050),
      locale: Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        _usuario.fechaNacimiento = picked;
        String day = picked.day.toString().padLeft(2, '0');
        String month = picked.month.toString().padLeft(2, '0');
        String year = picked.year.toString();
        _fechaController.text = '$day-$month-$year';
      });
    }
  }

  Widget createInputTelefono() {
    return TextFormField(
      initialValue: _usuario.telefono,
      autofocus: false,
      maxLength: 9,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.phone, color: kSecondaryColor),
        labelText: 'Teléfono',
        hintText: 'Ingrese su número telefónico',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (value.length < 7) {
          return 'Número telefónico no válido';
        }
        return validateTelefono(value);
      },
      onChanged: (value) => _usuario.telefono = value,
    );
  }

  String validateTelefono(String value) {
    Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null) {
      return 'Número telefónico no válido';
    }
    return null;
  }

  Widget createInputCorreo() {
    return TextFormField(
      initialValue: _usuario.correo,
      autofocus: false,
      maxLength: 50,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.email, color: kSecondaryColor),
        labelText: 'Correo',
        hintText: 'Ingrese su correo electrónico',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return validateEmail(value);
      },
      onChanged: (value) => _usuario.correo = value,
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null) {
      return 'Correo electrónico no válido';
    }
    return null;
  }

  updateUsuario() async {
    if (_formKey.currentState.validate()) {
      final response = await _usuarioService.updateUsuario(_usuario);
      if (response) {
        _prefs.saveData(_usuario);
        showSnack(Icons.thumb_up, 'Actualización exitosa!');
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } else {
        showSnack(Icons.cancel_outlined, 'Error al actualizar sus datos');
      }
    } else {
      showSnack(Icons.warning_amber_rounded, 'Ingrese sus datos correctamente');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        'Actualizar Datos',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
