import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/providers/evaluacion-alumno-provider.dart';
import 'package:edu_games/providers/evaluacion-provider.dart';
import 'package:edu_games/providers/juego-provider.dart';
import 'package:edu_games/screens/evaluacion/CreateEvaluationPage.dart';
import 'package:edu_games/screens/evaluacion/MyEvaluationList.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/widgets/OutlineActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EvaluationPage extends StatefulWidget {
  static final String routName = 'evaluation';

  @override
  _EvaluationPageState createState() => _EvaluationPageState();
}

class _EvaluationPageState extends State<EvaluationPage> {
  final SharedPreferenceService _prefs = SharedPreferenceService();

  @override
  Widget build(BuildContext context) {
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final evaAlumnoProvider = Provider.of<EvaluacionAlumnoProvider>(context);
    final juegoProvider = Provider.of<JuegoProvider>(context);
    juegoProvider.getJuegos();
    

    if (_prefs.userRole == 'alumno') {
      return Container(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Mis Evaluaciones',
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: FutureBuilder(
                  future: evaAlumnoProvider.getEvaluacionesActivasByAlumno(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<EvaluacionAlumno>> snapshot) {
                    if (snapshot.hasData) {
                      final evaluaciones = snapshot.data;
                      return evaluaciones.length == 0
                          ? Center(
                              child: Text('No tiene evaluaciones pendientes'),
                            )
                          : MyEvaluationList(
                              evaluacionesAlumno: evaluaciones,
                              role: _prefs.userRole,
                            );
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Evaluaciones',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  OutlineActionButton(
                    text: 'Agregar',
                    onPressedAction: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CreateEvaluationPage(
                            rolePage: 'create',
                          ),
                        ),
                      ).then((value) => setState(() {
                            //evaluacionProvider
                          }));
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              Expanded(
                child: FutureBuilder(
                  future: evaluacionProvider.getQueryEvaluacionesByDocente(''),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Evaluacion>> snapshot) {
                    if (snapshot.hasData) {
                      final evaluaciones = snapshot.data;
                      return MyEvaluationList(
                          evaluacionesDocente: evaluaciones,
                          role: _prefs.userRole);
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
