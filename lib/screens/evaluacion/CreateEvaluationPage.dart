import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/models/Grupo.dart';
import 'package:edu_games/models/Juegos.dart';
import 'package:edu_games/providers/evaluacion-provider.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/providers/juego-provider.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/utils/date-util.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateEvaluationPage extends StatefulWidget {
  static final String routName = 'create-evaluation';
  final Evaluacion evaluacion;
  final String rolePage;

  CreateEvaluationPage({this.evaluacion, this.rolePage});

  @override
  _CreateEvaluationPageState createState() => _CreateEvaluationPageState();
}

class _CreateEvaluationPageState extends State<CreateEvaluationPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _fechaController = TextEditingController();
  TextEditingController _horaController = TextEditingController();
  Evaluacion _evaluacion;
  List<String> _days = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
  String _daySelected;

  @override
  void initState() {
    /* _evaluacion = widget.evaluacion ?? Evaluacion();
    _daySelected = _evaluacion.dia == null ? _days[0] : _evaluacion.dia;
    _evaluacion.dia = _evaluacion.dia ?? _daySelected; */
    //_horaController.text = _evaluacion.hora ?? '';
    if (widget.rolePage == 'create') {
      _evaluacion = Evaluacion();
      _daySelected = _days[0];
      _evaluacion.dia = _daySelected;
      _fechaController.text = '';
      _horaController.text = '';
    } else {
      _evaluacion = widget.evaluacion;
      _daySelected = _evaluacion.dia;  
      String day = _evaluacion.fechaISO.day.toString().padLeft(2, '0');
      String month = _evaluacion.fechaISO.month.toString().padLeft(2, '0');
      String year = _evaluacion.fechaISO.year.toString();
      _fechaController.text = '$day-$month-$year';
      _horaController.text = _evaluacion.hora;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final juegoProvider = Provider.of<JuegoProvider>(context);

    return SafeArea(
      child: Scaffold(
        appBar: _TitleBar(
          rolePage: widget.rolePage,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        createInputNombre(),
                        SizedBox(height: 24),
                        FutureBuilder(
                          future: grupoProvider.getGruposByDocente(),
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Grupo>> snapshot) {
                            if (snapshot.hasData) {
                              return createInputGrupo(grupoProvider.grupos);
                            }
                            return SizedBox(height: 30);
                          },
                        ),
                        SizedBox(height: 24),
                        createInputFecha(),
                        SizedBox(height: 24),
                        createInputHoraEva(),
                        SizedBox(height: 24),
                        /* deprecated por requerimientos de los jp */
                        /*createInputDay(),
                        SizedBox(height: 24),*/
                        FutureBuilder(
                          future: juegoProvider.getJuegos(),
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Juego>> snapshot) {
                            if (snapshot.hasData) {
                              final juegos = snapshot.data;
                              return createInputJuego(juegos);
                            } else {
                              return SizedBox(height: 30);
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  Center(
                    child: ActionButton(
                      text: widget.rolePage == 'create' ? 'Crear' : 'Editar',
                      fontSize: 16,
                      onPressedAction: () {
                        saveEvaluacion(evaluacionProvider);
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget createInputNombre() {
    return TextFormField(
      autofocus: false,
      initialValue: _evaluacion.nombre ?? '',
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.paste, color: kSecondaryColor),
        labelText: 'Evaluación',
        hintText: 'Ingrese nombre de evaluación',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _evaluacion.nombre = value,
    );
  }

  Widget createInputGrupo(List<Grupo> grupos) {
    return FormField(
      initialValue: _evaluacion.grupoId ?? grupos[0].id,
      onSaved: (String value) {
        _evaluacion.grupoId = value;
      },
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            icon: Icon(Icons.group, color: kSecondaryColor),
            labelText: 'Grupo',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
          //isEmpty: _grupoSelected == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: _evaluacion.grupoId ?? grupos[0].id,
              onChanged: (value) {
                FocusScope.of(context).requestFocus(FocusNode());
                _evaluacion.grupoId = value;
                state.didChange(value);
              },
              items: grupoDropDownItems(grupos),
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<String>> grupoDropDownItems(List<Grupo> grupos) {
    List<DropdownMenuItem<String>> items = [];
    grupos.forEach((opt) {
      items.add(
        DropdownMenuItem<String>(child: Text(opt.nombre), value: opt.id),
      );
    });
    return items;
  }

  Widget createInputFecha() {
    return TextFormField(
      controller: _fechaController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        hintText: 'Fecha de Evaluación',
        labelText: 'Fecha de Evaluación',
        icon: Icon(Icons.today, color: kSecondaryColor),
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if (_evaluacion.fechaISO.year != DateTime.now().year) {
          return 'Fecha de evaluación no válido';
        }
        if (_evaluacion.fechaISO.isBefore(DateTime.now())) {
          return 'Fecha de evaluación no válido';
        }
        if(_evaluacion.fechaISO.weekday == 6 || _evaluacion.fechaISO.weekday == 7) {
          return 'Fecha de evaluación no válido';
        }
        return null;
      },
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        selectDate(context);
      },
    );
  }

  selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 1),
      lastDate: DateTime(DateTime.now().year + 1),
      locale: Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        _evaluacion.fechaISO = picked;
        _evaluacion.dia = mapWeekDay[picked.weekday];
        String day = picked.day.toString().padLeft(2, '0');
        String month = picked.month.toString().padLeft(2, '0');
        String year = picked.year.toString();
        _fechaController.text = '$day-$month-$year';
      });
    }
  }

  Widget createInputHoraEva() {
    return TextFormField(
      controller: _horaController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        hintText: 'Hora de Evaluación',
        labelText: 'Hora de Evaluación',
        icon: Icon(Icons.timer, color: kSecondaryColor),
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (_horaController.text.length < 1) {
          return 'Este campo es obligatorio';
        }
        /*if (int.parse(_evaluacion.hora.substring(0, 2)) < 8 ||
            int.parse(_evaluacion.hora.substring(0, 2)) > 18) {
          return 'Hora de la evaluación no válido';
        }*/
        if(_evaluacion.timeISO.hour < 8 || _evaluacion.timeISO.hour > 18) {
          return 'Hora de la evaluación no válido';
        }
        return null;
      },
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        selectTime(context);
      },
    );
  }

  selectTime(BuildContext context) async {
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 8, minute: 00),
    );

    if (picked != null) {
      setState(() {
        _evaluacion.timeISO = picked;
        final formatedTime = formatTimeUtil(picked);
        _evaluacion.hora = formatedTime;
        _horaController.text = formatedTime;
      });
    }
  }

  /* deprecadted por requerimientos volatiles de los jp */
  Widget createInputDay() {
    return InputDecorator(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
        icon: Icon(Icons.today, color: kSecondaryColor),
        labelText: 'Día de Evaluación',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(color: kPrimaryColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(color: kPrimaryColor),
        ),
      ),
      child: Container(
        margin: EdgeInsets.only(top: 8.0),
        height: 28.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            ...List.generate(
              _days.length,
              (index) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Container(
                  width: 28.0,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _daySelected = _days[index];
                        _evaluacion.dia = _daySelected;
                      });
                    },
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        _days[index].substring(0, 1),
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _daySelected == _days[index]
                        ? kSecondaryColor
                        : Colors.blueGrey[200],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  /* --- */

  Widget createInputJuego(List<Juego> juegos) {
    return FormField(
      initialValue: _evaluacion.juegoId ?? juegos[0].id,
      onSaved: (value) {
        _evaluacion.juegoId = value;
      },
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            icon: Icon(Icons.group, color: kSecondaryColor),
            labelText: 'Juego',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: kPrimaryColor),
            ),
          ),
          //isEmpty: _juegoSelected == '',
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: _evaluacion.juegoId ?? juegos[0].id,
              onChanged: (value) {
                FocusScope.of(context).requestFocus(FocusNode());
                _evaluacion.juegoId = value;
                state.didChange(value);
              },
              items: juegoDropDownItems(juegos),
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<String>> juegoDropDownItems(List<Juego> juegos) {
    List<DropdownMenuItem<String>> items = [];
    juegos.forEach((opt) {
      items.add(
        DropdownMenuItem<String>(child: Text(opt.nombre), value: opt.id),
      );
    });
    return items;
  }

  saveEvaluacion(EvaluacionProvider evaluacionProvider) async {
    if (_formKey.currentState.validate()) {
      bool response;
      _formKey.currentState.save();
      if (widget.rolePage == 'create')
        response = await evaluacionProvider.createEvaluacion(_evaluacion);
      else
        response = await evaluacionProvider.updateEvaluacion(_evaluacion);
      if (response) {
        final msg = widget.rolePage == 'create'
            ? 'Evaluación creada con éxito'
            : 'Evaluación actualizada con éxito';
        showSnack(Icons.thumb_up, msg);
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } else {
        final msg = widget.rolePage == 'create'
            ? 'Error al registrar la evaluación'
            : 'Error al actualizar la evaluación';
        showSnack(Icons.cancel_outlined, msg);
      }
    } else {
      showSnack(
          Icons.warning_amber_rounded, 'Complete los campos correctamente');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _TitleBar extends StatelessWidget with PreferredSizeWidget {
  final String rolePage;

  _TitleBar({this.rolePage});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        rolePage == 'create' ? 'Crear Evaluación' : 'Editar Evaluación',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 28.0,
          color: kPrimaryColor,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}
