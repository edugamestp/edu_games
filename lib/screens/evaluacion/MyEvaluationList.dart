import 'package:edu_games/models/Evaluacion.dart';
import 'package:edu_games/models/EvaluacionAlumno.dart';
import 'package:edu_games/providers/evaluacion-provider.dart';
import 'package:edu_games/screens/evaluacion/CreateEvaluationPage.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:edu_games/widgets/TileEvaluation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MyEvaluationList extends StatelessWidget {
  final List<Evaluacion> evaluacionesDocente;
  final List<EvaluacionAlumno> evaluacionesAlumno;
  final String role;

  MyEvaluationList({
    this.evaluacionesDocente,
    this.evaluacionesAlumno,
    this.role,
  });

  @override
  Widget build(BuildContext context) {
    return role == 'alumno'
        ? ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: evaluacionesAlumno.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TileEvaluation(
                  evaluacion: evaluacionesAlumno[index].nombreEvaluacion,
                  estado: evaluacionesAlumno[index].estado,
                  juego: evaluacionesAlumno[index].juegoId,
                  role: role,
                  onPressedAction: () {
                    Navigator.pushNamed(
                      context,
                      evaluacionesAlumno[index].juegoId,
                      arguments: <String, String>{
                        'evaAlumnoId': evaluacionesAlumno[index].id
                      },
                    );
                  },
                ),
              );
            },
          )
        : ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: evaluacionesDocente.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TileEvaluation(
                  evaluacion: evaluacionesDocente[index].nombre,
                  estado: evaluacionesDocente[index].estado,
                  juego: evaluacionesDocente[index].juegoId,
                  role: role,
                  onPressedAction: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return actions(evaluacionesDocente[index], context);
                        });
                  },
                ),
              );
            },
          );
  }

  Widget actions(Evaluacion evaluacion, BuildContext context) {
    return Container(
      height: 230,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0),
            child: Text('${evaluacion.nombre}'),
          ),
          Divider(),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(
                      FontAwesomeIcons.solidEdit,
                      color: kPrimaryColor,
                    ),
                  ),
                  Text('Editar evaluación'),
                ],
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CreateEvaluationPage(
                    evaluacion: evaluacion,
                    rolePage: 'edit',
                  ),
                ),
              );
            },
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(
                      FontAwesomeIcons.trash,
                      color: kPrimaryColor,
                    ),
                  ),
                  Text('Eliminar evaluación'),
                ],
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return _DialogDeleteEvaluacion(evaluacion: evaluacion);
                  });
            },
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 36.0,
                    child: FaIcon(
                      FontAwesomeIcons.playCircle,
                      color: kPrimaryColor,
                    ),
                  ),
                  Text('Activar evaluación'),
                ],
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return _DialogActivateEvaluacion(evaluacion: evaluacion);
                  });
            },
          ),
        ],
      ),
    );
  }
}

class _DialogDeleteEvaluacion extends StatelessWidget {
  final Evaluacion evaluacion;

  _DialogDeleteEvaluacion({this.evaluacion});

  @override
  Widget build(BuildContext context) {
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        height: 200.0,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Atención!',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '¿Está seguro de eliminar la evaluación?',
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ActionButton(
                    text: 'Sí',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () async {
                      final wasDeleted =
                          await evaluacionProvider.deleteEvaluacion(evaluacion);
                      if (wasDeleted)
                        showSnack(Icons.thumb_up,
                            'Evaluación eliminada con éxito', context);
                      else
                        showSnack(Icons.cancel_outlined,
                            'Error al eliminar la evaluación', context);
                      Navigator.pop(context);
                    },
                  ),
                  ActionButton(
                    text: 'No',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  showSnack(IconData icon, String message, BuildContext context) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}

class _DialogActivateEvaluacion extends StatelessWidget {
  final Evaluacion evaluacion;

  _DialogActivateEvaluacion({this.evaluacion});

  @override
  Widget build(BuildContext context) {
    final evaluacionProvider = Provider.of<EvaluacionProvider>(context);
    final mediaSize = MediaQuery.of(context).size;

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        width: mediaSize.width,
        height: 200.0,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Atención!',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '¿Está seguro que desea activar la evaluación?',
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ActionButton(
                    text: 'Sí',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () async {
                      final wasActivated = await evaluacionProvider
                          .activateEvaluacion(evaluacion);
                      if (wasActivated)
                        showSnack(Icons.thumb_up,
                            'Evaluación activada con éxito', context);
                      else
                        showSnack(Icons.cancel_outlined,
                            'Error al activar la evaluación', context);
                      Navigator.pop(context);
                    },
                  ),
                  ActionButton(
                    text: 'No',
                    minWidth: 100,
                    minHeight: 36,
                    fontSize: 16,
                    onPressedAction: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  showSnack(IconData icon, String message, BuildContext context) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}
