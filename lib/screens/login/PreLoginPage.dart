import 'package:edu_games/screens/login/LoginPage.dart';
import 'package:edu_games/screens/register/SelectRolePage.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:flutter/material.dart';

class PreLoginPage extends StatelessWidget {
  const PreLoginPage({Key key}) : super(key: key);
  static final String routName = 'pre-login';

  @override
  Widget build(BuildContext context) {
    final mediaSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Stack(children: [
            Align(
              alignment: Alignment(0, -0.8),
              child: Image.asset('images/logo-edu-games.jpg'),
            ),
            Align(
              alignment: Alignment(0, 0.8),
              child: Container(
                height: mediaSize.height * 0.20,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ActionButton(
                      text: 'Iniciar Sesión',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        Navigator.of(context).pushNamed(LoginPage.routName);
                      },
                    ),
                    ActionButton(
                      text: 'Registrarse',
                      minWidth: 180,
                      minHeight: 36,
                      fontSize: 16,
                      onPressedAction: () {
                        Navigator.of(context).pushNamed(SelectRolePage.routName);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
