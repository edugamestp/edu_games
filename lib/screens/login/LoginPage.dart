import 'package:edu_games/models/Usuario.dart';
import 'package:edu_games/providers/grupo-provider.dart';
import 'package:edu_games/screens/main/MainPage.dart';
import 'package:edu_games/services/auth-service.dart';
import 'package:edu_games/services/shared-preference-service.dart';
import 'package:edu_games/utils/constants.dart';
import 'package:edu_games/widgets/ActionButton.dart';
import 'package:edu_games/widgets/CustomSnack.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  static final String routName = 'login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final prefs = SharedPreferenceService();
  final _formKey = GlobalKey<FormState>();
  final AuthService _authService = AuthService();
  Usuario _usuario = Usuario();

  @override
  Widget build(BuildContext context) {
    final grupoProvider = Provider.of<GrupoProvider>(context);    

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container( 
          color: Colors.white,       
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 60.0, bottom: 32.0),
                child: Image.asset('images/logo-edu-games.jpg'),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 24, horizontal: 12),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        createInputDni(),
                        SizedBox(height: 24),
                        createInputContrasenya(),
                        ActionButton(
                          text: 'Iniciar Sesión',
                          primaryColor: kPrimaryColor,
                          contrastColor: Colors.white,
                          minWidth: 180,
                          minHeight: 36,
                          fontSize: 16,
                          onPressedAction: () async {
                            await loginUsuario();
                            grupoProvider.getGruposByDocente();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget createInputDni() {
    return TextFormField(
      autofocus: false,
      maxLength: 8,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.call_to_action, color: kSecondaryColor),
        labelText: 'DNI',
        hintText: 'Ingrese su DNI',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        if(value.length != 8) {
          return 'El campo DNI debe tener 8 dígitos';
        }
        return null;
      },
      onChanged: (value) => _usuario.dni = value,
    );
  }

  Widget createInputContrasenya() {
    return TextFormField(
      autofocus: false,
      maxLength: 20,
      obscureText: true,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(8.0),
        icon: Icon(Icons.lock, color: kSecondaryColor),
        labelText: 'Contraseña',
        hintText: 'Ingrese su contraseña',
        hintStyle: TextStyle(color: Colors.grey[400]),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Este campo es obligatorio';
        }
        return null;
      },
      onChanged: (value) => _usuario.contrasenya = value,
    );
  }

  loginUsuario() async {
    if (_formKey.currentState.validate()) {
      final response = await _authService.loginUsuario(_usuario);
      if (response != null) {
        prefs.saveData(response);
        /* setState(() {          
        }); */
        Navigator.pushNamedAndRemoveUntil(
            context, MainPage.routName, (route) => false);
      } else {
        showSnack(Icons.cancel_outlined,
            'La Contraseña o DNI ingresados son incorrectos');
      }
    } else {
      showSnack(Icons.warning_amber_rounded, 'Complete los campos requeridos');
    }
  }

  showSnack(IconData icon, String message) {
    final snack = customSnack(icon: icon, message: message);
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}
